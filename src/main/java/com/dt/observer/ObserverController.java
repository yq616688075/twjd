package com.dt.observer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
@RestController
@RequestMapping(value = "/observer")
public class ObserverController {
     @Autowired
     private ObserverCenter observerCenter;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public void test(){
      observerCenter.notice(ObserverEnum.TEST_OBSERVER,"1");
    }
}
