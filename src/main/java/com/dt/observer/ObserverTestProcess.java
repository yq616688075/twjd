package com.dt.observer;

import com.sun.org.apache.xpath.internal.Arg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
@Slf4j
@Component
public class ObserverTestProcess implements  ObserverHandler {
    @Override
    public void execute(Object agrs) {
       log.info("观察者测试处理,参数{}", agrs.toString());
    }

    @Override
    public Enum getTopic() { return ObserverEnum.TEST_OBSERVER; }
}
