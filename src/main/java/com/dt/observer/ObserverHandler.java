package com.dt.observer;

/**
 * 观察者处理器
 *
 * 观察者接收通知时以topic为关键字寻找实现，因此
 * 在有多个SaasObserverHandler实现时
 * 若指定了类型E，则为约定，所有观察者实现SaasObserverHandler的execute都需要执行类型E，否则会出现类型转换错误
 * 若没有指定类型E，则E为Object，那么观察者在实现SaasObserverHandler的execute都实现自行进行强制转换
 *
 * 指定E例子：
 * public class XXX implements SaasObserverHandler<Vo1> {
 *     public void execute(Vo1 arg) { ... }
 *     public Topic1 getTopic() { ... }
 * }
 *
 * 无指定E例子：
 * public class XXX implements SaasObserverHandler {
 *     public void execute(Object arg) {
 *         if (!(arg instanceof Vo1)) {
 *             return;
 *         }
 *         Vo1 vo1 = (Vo1)arg;
 *         ...
 *     }
 *     public Topic1 getTopic() { ... }
 * }
 *
 * 使用时，
 * 若指定了E，请在Topic上说明对应的具体类型E，方便观察者的实现
 * 没有指定E，请在Topic上说明可能传参E，方便观察者的实现
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
public interface ObserverHandler<E extends  Object> {
     void execute(E agrs);

     <T extends Enum<T>> T getTopic();
}
