package com.dt.observer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
@Component
@Slf4j
public class ObserverCenter implements ApplicationListener<ApplicationReadyEvent> {

    /**
     * key 为主题,value为观察者列表
     */
    private Map<Object, List<Object>> subjectMap=new HashMap<>();


    public <T extends Enum>  void notice(T topic, Object arg){
        if(this.subjectMap.isEmpty()){
            return;
        }
        List<Object> observerHandlers = this.subjectMap.get(topic);
        if(CollectionUtils.isEmpty(observerHandlers)){
            return;
        }
        for (Object observerHandler : observerHandlers) {
             if(observerHandler instanceof ObserverHandler){
               log.info("主题：{}.{}，通知观察者：{}，通知参数：{}", topic.getClass().getName(), topic, observerHandler.getClass().getName(), arg);
                 ObserverHandler handler = (ObserverHandler) observerHandler;
                 handler.execute(arg);
             }
        }
    }


    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        ApplicationContext context = applicationReadyEvent.getApplicationContext();
        //扫描观察者
        Map<String, ObserverHandler> observerBeansMap = context.getBeansOfType(ObserverHandler.class);
        if(null==observerBeansMap||observerBeansMap.isEmpty()){
             return;
        }
        for (Map.Entry<String, ObserverHandler> entry : observerBeansMap.entrySet()) {
            ObserverHandler observerHandler = entry.getValue();
            Object topic = observerHandler.getTopic();
            List<Object> observerHandlers = this.subjectMap.get(topic);
            if(null==observerHandlers){
                observerHandlers = new ArrayList<>();
                this.subjectMap.put(topic,observerHandlers);
            }
            observerHandlers.add(observerHandler);
        }
    }
}
