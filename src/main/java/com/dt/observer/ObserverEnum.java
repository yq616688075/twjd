package com.dt.observer;


/**
 * 观察者枚举
 */
public enum ObserverEnum {
   TEST_OBSERVER(1,"观察者模式测试");

   private  Integer code;
   private  String msg;

    ObserverEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer code() {
        return code;
    }

    public String msg(){
        return  msg;
    }
}
