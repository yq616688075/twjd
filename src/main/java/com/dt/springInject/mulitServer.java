package com.dt.springInject;

import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
public interface mulitServer {
    void  getName();
}

@Component
class mulitServerImpl1 implements mulitServer{
    @Override
    public void getName() {
        System.out.println("mulitServerImpl1");
    }
}
@Component
class mulitServerImpl2 implements mulitServer{
    @Override
    public void getName() {
        System.out.println("mulitServerImpl2");
    }
}

@Component
class mulitServerImpl3 implements mulitServer{
    @Override
    public void getName() {
        System.out.println("mulitServerImpl3");
    }
}

