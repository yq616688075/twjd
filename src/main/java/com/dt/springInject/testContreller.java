package com.dt.springInject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
@RestController
@RequestMapping("/test")
public class testContreller {
    @Autowired
    private List<mulitServer> mulitServer;

    @Autowired
    private Map<String,mulitServer> mulitServerMap;

    @RequestMapping("/t")
    public void test(HttpServletRequest request, HttpServletResponse response) {
        boolean empty = CollectionUtils.isEmpty(mulitServer);
        if(!empty){
            for (com.dt.springInject.mulitServer server : mulitServer) {
                 server.getName();
            }
        }
        boolean empty1 = CollectionUtils.isEmpty(mulitServerMap);
        if (!empty1){
            for (Map.Entry<String, com.dt.springInject.mulitServer> entry : mulitServerMap.entrySet()) {
                System.out.println("key:"+entry.getKey());
                entry.getValue().getName();
            }
        }
    }
}
