package com.dt.thread;

import sun.reflect.generics.tree.Tree;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-23
 */
public class MyBlockingQueue {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        ExecutorService executorService2 = Executors.newSingleThreadExecutor();
        BlockingQueue blockingQueue = new ArrayBlockingQueue(2);
        executorService.submit(() -> {
            try {
                for (int i = 0; i < 20; i++) {
                    blockingQueue.put(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executorService2.submit(() -> {
            try {
                while (true){
                    Thread.sleep(3000);
                    Object take = blockingQueue.take();
                    System.out.println(take.toString());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }
}

