package com.dt.thread;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-22
 */
public class ImplementsRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("run-Thread");
    }
}

class test3{
    public static void main(String[] args) {
        new Thread(new ImplementsRunnable()).start();
    }
}
