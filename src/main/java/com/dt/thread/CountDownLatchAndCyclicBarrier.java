package com.dt.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description:
 *    CountDownLatch和CyclicBarrier使用
 *    CountDownLatch是一个计数器，线程完成一个记录一个，计数器递减，只能只用一次
 *    CyclicBarrier的计数器更像一个阀门，需要所有线程都到达，然后继续执行，计数器递增，提供Reset功能，可        以多次使用。
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-23
 */
public class CountDownLatchAndCyclicBarrier {
    public static void main(String[] args) {
        //创建CountDownLatch
        CountDownLatch countDownLatch = new CountDownLatch(2);
        //创建固定线程
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(() -> {
            try {
                //睡3秒后线程countDownLatch-1
                Thread.sleep(3000);
                System.out.println(Thread.currentThread().getName() + 1);
                countDownLatch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executorService.submit(() -> {
            //countDownLatch直接减
            System.out.println(Thread.currentThread().getName() + 2);
            countDownLatch.countDown();
        });
        try {
            //阻塞当前线程,直到计数器的值为0
            countDownLatch.await();
            //结果会等待3秒后执行主线程
            System.out.println(Thread.currentThread().getName() + "主线程");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
