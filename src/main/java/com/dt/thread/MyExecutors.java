package com.dt.thread;

import java.util.Date;
import java.util.concurrent.*;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-22
 */
public class MyExecutors {
    public static void main(String[] args) {
        //1.newScheduledThreadPool定长线程池，支持定时及周期性任务执行
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
        ScheduledFuture<?> scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(() -> {
            System.out.println(new Date().getTime());
        }, 1, 3, TimeUnit.SECONDS);

        //2.newFixedThreadPool创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 100; i++) {
            int finalI = i;
            executorService.submit(()->{
                try {
                    Thread.sleep(5000);
                    System.out.println(finalI);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        //3. newCachedThreadPool 创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程
        //线程池为无限大，当执行第二个任务时第一个任务已经完成，会复用执行第一个任务的线程，而不用每次新建线程
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < 100; i++) {
            cachedThreadPool.submit(()-> System.out.println(Thread.currentThread().getName()));
        }

       //4.newSingleThreadExecutor创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序（FIFO, LIFO, 优先级）执行
        ExecutorService singleThreadService = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 20; i++) {
            singleThreadService.submit(() -> System.out.println(Thread.currentThread().getName()));
        }
    }
}