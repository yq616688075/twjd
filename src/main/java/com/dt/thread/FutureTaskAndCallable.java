package com.dt.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @description: 使用FutureTask创建线程,FutureTask其实也是Runnable
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-22
 */
public class FutureTaskAndCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask futureTask = new FutureTask<>(new MyCallable());
        new Thread(futureTask).start();
        //调用get方法会阻塞线程
        Object o = futureTask.get();
        System.out.println(o.toString());

    }
}

class MyCallable implements Callable{
    @Override
    public Object call() throws Exception {
        return Thread.currentThread().getName();
    }
}
