package com.dt.lambdaAndStreamAndFuncation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
public class FlatMapTest {
    public static void main(String[] args) {
        //创建list
        List<String> list = Stream.of("build,st", "build,ffd", "build,sa").collect(Collectors.toList());
        //用[,]分割,并且去重
        //把多个小的stream放入一个大的stream里面
        List<String> collect = list.stream().flatMap(str -> Arrays.stream(str.split(","))).distinct().collect(Collectors.toList());
        System.out.println(collect);
    }
}
