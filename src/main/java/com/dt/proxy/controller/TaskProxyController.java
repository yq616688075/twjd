package com.dt.proxy.controller;

import com.dt.proxy.proxy.CGLIBDynaProxy;
import com.dt.proxy.proxy.JDKDynaProxy;
import com.dt.proxy.service.TaskService;
import com.dt.proxy.service.impl.TaskServiceImpl;
import com.dt.proxy.vo.Task;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */

@RestController
@RequestMapping(value = "/taskProxy")
public class TaskProxyController {

    @RequestMapping(value = "taskJDKProxy",method = RequestMethod.GET)
    public void taskJDKProxyPro(){
        //JDK代理,代理对象会实现被代理实现的接口,所以必须用代理对象实现的接口接收
        TaskService taskService = (TaskService) new JDKDynaProxy().bind(new TaskServiceImpl());
        Task task = taskService.selectTaskByCode(200);
        System.out.println(task);
    }

    @RequestMapping(value = "taskCGLIBProxy",method = RequestMethod.GET)
    public void taskCGLIBProxyPro(){
        //CGLIB代理,代理对象继承被代理对象,可用代理对象接收
        TaskServiceImpl taskService = (TaskServiceImpl) new CGLIBDynaProxy().bind(new TaskServiceImpl());
        Task task = taskService.selectTaskByCode(100);
    }
}
