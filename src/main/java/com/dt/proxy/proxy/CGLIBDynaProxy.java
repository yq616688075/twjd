package com.dt.proxy.proxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @description: CGLIBD动态代理
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-17
 */
public class CGLIBDynaProxy implements MethodInterceptor {
    //被代理对象
    private Object targetObject;

    //返回代理对象
    public  Object bind(Object object){
       this.targetObject=object;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(object.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("CGLIB前置处理");
        Object invoke = method.invoke(targetObject, objects);
        System.out.println("CGLIB后置处理");
        return  invoke;
    }
}
