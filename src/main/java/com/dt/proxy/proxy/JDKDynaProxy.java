package com.dt.proxy.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @description: JDK动态代理类
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
public class JDKDynaProxy implements InvocationHandler {
    //需要代理的目标对象
    private Object targetObject;

    //将目标对象传入进行代理
    public Object bind(Object object) {
        this.targetObject = object;
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), object.getClass().getInterfaces(), this);
    }

    //执行方法
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("前置处理.....");
        Object invoke = method.invoke(targetObject, args);
        System.out.println("后置处理.....");
        return invoke;
    }
}
