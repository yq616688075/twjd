package com.dt.proxy.service.impl;

import com.dt.proxy.service.TaskService;
import com.dt.proxy.vo.Task;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
@Service
public class TaskServiceImpl implements TaskService {
    @Override
    public Task selectTaskByCode(Integer code) {
        Task test = new Task().setId(100L).setContent("测试任务").setCode(code);
        System.out.println(test);
        return test;
    }
}
