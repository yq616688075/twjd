package com.dt.proxy.service;

import com.dt.proxy.vo.Task;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
public interface TaskService {
    Task selectTaskByCode(Integer code);
}
