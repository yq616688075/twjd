package com.dt.proxy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @description:  任务vo
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Task {

    /**
     * id
     */
    private Long id;

    /**
     * 内容
     */
    private String content;


    /**
     * 任务code
     */
    private Integer code;

}


