package com.dt.export.supplier;

import java.util.function.Supplier;

public interface DataSupplier<T> extends Supplier<T> {
}
