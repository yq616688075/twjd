package com.dt.export.util;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */

import java.io.Closeable;
import java.net.HttpURLConnection;

/**
 * 关闭资源工具类，资源关闭的同时也会被设置为null
 * <p>备注: 可以关闭JDK里面绝大多数需要关闭的资源对象,同时不对外抛出异常
 * <p>  如果第三方需要关闭的资源类遵循jdk的接口标准也可以关闭,主要使用接口Closeable;
 */
public final class CloseUtil {

    public static void closeSilently(Closeable rsc) {
        if (null != rsc) {
            try { rsc.close(); } catch (Exception ex) { /* 消除异常 */ }
        }
    }

    public static void closeSilently(HttpURLConnection conn) {
        if (null != conn) {
            try { conn.disconnect(); } catch (Exception ex) { /* 消除异常 */ }
        }
    }

    public static void closeSilently(Process process) {
        if (null != process) {
            try { process.destroy(); } catch (Exception ex) { /* 消除异常 */ }
        }
    }

}