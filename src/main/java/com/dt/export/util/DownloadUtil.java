package com.dt.export.util;

import com.dt.export.ByteToInputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.EOFException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * 下载文件，目前仅支持get方式
 * Created by liangjj15 on 2019/1/18
 */
public class DownloadUtil {

    private final static Logger logger = LoggerFactory.getLogger(DownloadUtil.class);

    public static byte[] downloadEx(String urlDown, Map<String, String> requestProperties, int timeout) throws EOFException,Exception{
        HttpURLConnection conn = null;
        try {
            SslUtils.ignoreSsl();
            URL theURL = new URL(urlDown);
            conn = (HttpURLConnection) theURL.openConnection();
            if (requestProperties != null) {
                for (Map.Entry<String, String> props : requestProperties.entrySet()) {
                    conn.setRequestProperty(props.getKey(), props.getValue());
                }
            }
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(timeout);
            conn.setReadTimeout(timeout);

            int responseCode = conn.getResponseCode();
            conn.getContentLength();
            if (responseCode >= 400) {
                throw new Exception("http responseCode >= 400");
            }
            InputStream inputStream =  conn.getInputStream();
            return ByteToInputStream.input2byte(inputStream);
        }
        catch (EOFException e) {
            throw e;
        }
        catch (Exception e1) {
            throw e1;
        } finally {
            CloseUtil.closeSilently(conn);
        }
    }

    /**
     * 下载重试
     * @param urlDown
     * @param requestProperties
     * @param timeout
     * @return
     */
    public static byte[] retryDownload(String urlDown, Map<String, String> requestProperties, int timeout,Integer currentDownloadCount, Integer downloadRetryCount) {
        byte[] bytes = null;
        try {
            //隔一秒，重试一次
            Thread.sleep(1000);
            logger.info("downloadEx retry urlDown = {}",urlDown);
            currentDownloadCount++;
            bytes = downloadEx(urlDown, requestProperties, timeout);
        }
        catch (EOFException e2){
            if (currentDownloadCount <= downloadRetryCount) {
                bytes = retryDownload(urlDown, requestProperties, timeout,currentDownloadCount,downloadRetryCount);
            } else {
                logger.error("download {}",e2.toString());
            }
        }
        catch (Exception e1) {
            logger.error("download2 {}",e1.toString());
        }
        return bytes;
    }

    //下载eof出错重试2次,隔一秒重试一次
    public static byte[] download(String urlDown, Map<String, String> requestProperties, int timeout) {
        byte[] bytes = null;
        //下载重试出错
        Integer downloadRetryCount = 2;
        Integer currentDownloadCount = 0;
        try {
            //第一次下载
            currentDownloadCount++;
            bytes = downloadEx(urlDown, requestProperties, timeout);
        } catch (EOFException e) {
            bytes = retryDownload(urlDown, requestProperties, timeout,currentDownloadCount,downloadRetryCount);
        } catch (Exception e) {
            logger.error("download file url={} err={}",urlDown,e.toString());
        }
        return bytes;
    }

    public static String createDownloadFileEncodedFileName(HttpServletRequest request, String fileName) throws UnsupportedEncodingException {
        String userAgent = null;
        if (null != request) {
            userAgent = request.getHeader("User-Agent");
        }
        String fileNameEncode = URLEncoder.encode(fileName, "UTF-8");
        if (null != userAgent) {
            if (StringUtils.indexOfIgnoreCase(userAgent, "chrome") != -1
                    || StringUtils.indexOfIgnoreCase(userAgent, "safari") != -1) {
                fileNameEncode = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
            }
        }
        return fileNameEncode;
    }

    public static void createDownloadFileContentDispositionContent(HttpServletRequest request, HttpServletResponse response, String fileName) throws UnsupportedEncodingException {
        String fileNameEncode = DownloadUtil.createDownloadFileEncodedFileName(request, fileName);
        String userAgent = null;
        if (null != request) {
            userAgent = request.getHeader("User-Agent");
        }
        if (null != userAgent) {
            if (StringUtils.indexOfIgnoreCase(userAgent, "opera") != -1) {
                response.setHeader("Content-Disposition", "attachment; filename* = UTF-8''" + fileNameEncode);
                return;
            }
        }
        response.setHeader("Content-Disposition", "attachment; filename = " + fileNameEncode);
    }
}
