package com.dt.export;


import lombok.Getter;
import lombok.Setter;

/**
 * 列信息
 * @param <T>
 */
public class ColumnInfo<T> {

	/**
	 * 表头
	 */
	@Getter
	@Setter
	private String header;

	/**
	 * 导出字段
	 */
	@Getter
	private String filedName;

	/**
	 * 字段转换器
	 */
	@Getter
	private FieldConversion<T> fieldConversion;

	public ColumnInfo(String header, FieldConversion<T> fieldConversion){
		this.header = header;
		this.fieldConversion = fieldConversion;
	}

	public ColumnInfo(String header, String filedName) {
		this.header = header;
		this.filedName = filedName;
	}




}