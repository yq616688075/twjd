package com.dt.singleton;

/**
 * @description:
 * 懒汉式
 * 有线程安全问题,不推荐使用(可以使用synchronized同步方法,线程安全,但是效率低)
 * 原因分析,如果有多个线程同时进入,同时判断singletonLazy=null,就会产生多个实例
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-22
 */
public class SingletonLazy {
    private static SingletonLazy singletonLazy=null;

    private SingletonLazy(){};

 //   public static synchronized SingletonLazy getSingletonLazy(){
    public static SingletonLazy getInstance(){
        if (singletonLazy==null){
            return new SingletonLazy();
        }
        return singletonLazy;
    }

}
