package com.dt.singleton;


/**
 * 枚举方式创建单例
 * 枚举类型，无线程安全问题，避免反序列华创建新的实例，很少使用
 */
public enum SingletonEnum {
    INSTANCE;
}
