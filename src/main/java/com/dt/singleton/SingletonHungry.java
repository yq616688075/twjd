package com.dt.singleton;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @description:
 * 饿汉式
 * 无线程安全问题,不能延迟加载,影响系统性能
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-22
 */
public class SingletonHungry {
    private static final SingletonHungry singletonHungry = new SingletonHungry();

    private SingletonHungry() {
    }

    public static SingletonHungry getInstance() {
        return singletonHungry;
    }
}
