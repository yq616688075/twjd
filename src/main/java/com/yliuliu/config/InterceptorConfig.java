package com.yliuliu.config;

import com.yliuliu.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by admin on 2019/6/17.
 * 拦截器配置
 */
@Configuration
public class InterceptorConfig  implements WebMvcConfigurer {
    @Autowired
    private TokenInterceptor tokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(tokenInterceptor);
        //拦截配置
        registration.addPathPatterns("/twjd/**");
        //排除配置
        registration.excludePathPatterns("/twjd/login","/twjd/error");
    }
}
