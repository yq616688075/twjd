package com.yliuliu.model;

import lombok.*;

/**
 * Created by admin on 2019/6/13.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Sysuser {
    private   String ID;
    private  String name;
    private  String password;
    private  String address;
}
