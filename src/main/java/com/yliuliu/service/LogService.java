package com.yliuliu.service;

import com.yliuliu.model.Sysuser;
import com.yliuliu.redisLock.CacheLock;
import com.yliuliu.redisLock.LockObject;

/**
 * Created by admin on 2019/6/13.
 */
public interface LogService {
    //获取用户信息
    @CacheLock(lockPrefix ="redisLock_",timeOut =1000L,expireTime = 100L)
    public Sysuser getUser(@LockObject String userId);
}
