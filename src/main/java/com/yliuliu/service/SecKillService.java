package com.yliuliu.service;

import com.yliuliu.redisLock.CacheLock;
import com.yliuliu.redisLock.LockObject;

/**
 * Created by admin on 2019/7/30.
 */
public interface SecKillService {
    @CacheLock(lockPrefix ="redisLock_",timeOut =1000L,expireTime = 1000L)
    public Long secKill(String userId, @LockObject String productId);
}
