package com.yliuliu.service.impl;

import com.yliuliu.repository.ProductRep;
import org.springframework.stereotype.Service;
import com.yliuliu.service.SecKillService;

/**
 * Created by admin on 2019/7/30.
 */
@Service
public class SecKillServiceImpl implements SecKillService {
    @Override
    public Long secKill(String userId, String productId) {
        //模拟最简单的秒杀,就是商品减一
        Long aLong = ProductRep.inventory.get(productId);
        //判断库存是否>0
        if (aLong>0){
            //如果大于1则减掉一个库存
            ProductRep.inventory.put(productId,ProductRep.inventory.get(productId)-1);
        }
        //返回商品个数
        return aLong;
    }
}
