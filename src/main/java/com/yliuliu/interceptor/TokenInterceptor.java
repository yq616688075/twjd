package com.yliuliu.interceptor;

import com.auth0.jwt.interfaces.Claim;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import com.yliuliu.util.StringEmpty;
import com.yliuliu.util.TokenUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by admin on 2019/6/17.
 */
@Component
public class TokenInterceptor implements HandlerInterceptor {
   private   Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        //token验证
        if (!StringEmpty.IsEmpty(token)) {
            Map<String, Claim> claimMap = TokenUtil.verifyToken(token);
            if (claimMap!=null){
                //账户操作...
                return true;
            } else {
                //验证错误,跳转到错误页面
                response.sendRedirect(request.getContextPath()+"/twjd/error");
               return false;
            }
        }else{
            //验证错误,跳转到错误页面
            response.sendRedirect(request.getContextPath()+"/twjd/error");
            return false;
        }
    }
}
