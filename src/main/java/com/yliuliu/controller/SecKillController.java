package com.yliuliu.controller;

import com.yliuliu.redisLock.CacheLockDynaProxy;
import com.yliuliu.repository.ProductRep;
import com.yliuliu.service.SecKillService;
import com.yliuliu.service.impl.SecKillServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-3-30
 */
@RestController
@RequestMapping("/pc/api")
public class SecKillController {
    @RequestMapping(value = "/secKill")
    @ResponseBody
    public void secKill() {
        //模拟秒杀场景,开启一千个线程秒杀,五百个线程秒杀第一件商品,五百个线程秒杀第二件商品
        int threadCount=1000;
        int splitPoint=500;
        //使用CountDownLatch计数
        CountDownLatch endCount=new CountDownLatch(threadCount);
        CountDownLatch beginCount=new CountDownLatch(1);
        Thread[] threads = new Thread[threadCount];

        //开启五百个秒杀第一件商品
        for (int i=0;i<splitPoint;i++){
            threads[i]=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //等待在一个信号量上,挂起
                        beginCount.await();
                        //用动态代理的方法调用秒杀方法
                        SecKillService sec= (SecKillService) new CacheLockDynaProxy().bind(new SecKillServiceImpl());
                        Long aLong = sec.secKill(UUID.randomUUID().toString(), "10000001");
                        if(aLong!=null&&aLong>0){
                            System.out.println("电风扇:"+Thread.currentThread().getName()+"10000001 商品号:"+aLong);
                        }
                        //减少一个信号量
                        endCount.countDown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            threads[i].start();
        }
        //开启五百个秒杀第二件商品
        for (int j=0;j<splitPoint;j++){
            threads[j]=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //等待在一个信号量上,挂起
                        beginCount.await();
                        //用动态代理的方法调用秒杀方法
                        SecKillService sec= (SecKillService) new CacheLockDynaProxy().bind(new SecKillServiceImpl());
                        Long aLong = sec.secKill(UUID.randomUUID().toString(), "10000002");
                        if(aLong!=null&&aLong>0){
                            System.out.println("空调:"+Thread.currentThread().getName()+"10000002 商品号:"+aLong);
                        }
                        //减少一个信号量
                        endCount.countDown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            threads[j].start();
        }
        ////主线程释放开始信号量，并等待结束信号量，这样做保证1000个线程做到完全同时执行，保证测试的正确性
        beginCount.countDown();

        try {
            //主线程等待结束信号量
            endCount.await();
            //观察秒杀结果是否正确
            System.out.println("空调剩余"+ ProductRep.inventory.get("10000002"));
            System.out.println("电风扇剩余"+ProductRep.inventory.get("10000001"));
            System.out.println("--新增加数量,下一次秒杀准备---");
            ProductRep.inventory.put("10000002",10L);
            ProductRep.inventory.put("10000001",10L);
            System.out.println("--------------------end---------------------------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
