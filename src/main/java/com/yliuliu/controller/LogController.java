package com.yliuliu.controller;

import com.alibaba.fastjson.JSON;
import com.yliuliu.common.base.MapUtil;
import com.yliuliu.model.Sysuser;
import com.yliuliu.redisLock.CacheLockDynaProxy;
import com.yliuliu.service.LogService;
import com.yliuliu.service.impl.logServiceImpl;
import com.yliuliu.util.RedisUtil;
import com.yliuliu.util.StringEmpty;
import com.yliuliu.util.TokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by admin on 2019/6/13.
 */
@Controller
@RequestMapping(value = {"/pc/api"})
public class LogController {
    @Autowired
    private LogService logService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    private Logger logger = LoggerFactory.getLogger(LogController.class);

    /**
     * 用户登陆
     *
     * @param sysusers
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, String> log(Sysuser sysusers, HttpServletRequest request, HttpServletResponse response) {
        Sysuser user = logService.getUser(sysusers.getName());
        HashMap<String, String> tokenMap = new HashMap<>(1);
        //用户存在且密码正确在则颁发token
        if (null != user && user.getPassword().equals(sysusers.getPassword())) {
            if (!StringUtils.isEmpty(user.getPassword())) {
                response.setHeader("token", TokenUtil.sign(user.getID()));
                tokenMap.put("token", TokenUtil.sign(user.getID()));
                HttpSession session = request.getSession();
                session.setAttribute("name", user.getName());
                session.setAttribute("password", user.getPassword());
                //存入redis中设置过期时间1天
                if (!redisUtil.exists(sysusers.getName())) {
                    logger.info("活跃用户+1:{}", sysusers.getName());
                    redisUtil.set(sysusers.getName(), JSON.toJSONString(sysusers), 1, TimeUnit.DAYS);
                }
                return tokenMap;
            }
        } else {
            tokenMap.put("token", "不存在此用户");
        }
        return tokenMap;
    }


    @RequestMapping(value = "/querySession")
    @ResponseBody
    public HashMap<String, Object> querySession(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String, Object> sessionMap = new HashMap<>();
        HttpSession session = request.getSession();
        sessionMap.put("sessionId", session.getId());
        //sessionMap.put("name", session.getAttribute("name"));
        //sessionMap.put("name", session.getAttribute("name"));
        //sessionMap.put("name", session.getAttribute("name"));
        //sessionMap.put("name", session.getAttribute("name"));
        sessionMap.put("name", 1);
        return sessionMap;
    }

    /**
     * 增加用户
     *
     * @param sysusers
     * @return
     */
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public String addUser(Sysuser sysusers) {
        if (redisUtil.exists(sysusers.getName())) {
            redisUtil.set(sysusers.getName(), sysusers);
        }
        return "增加成功";
    }

    /**
     * 错误跳转页面
     *
     * @param sysusers
     * @return
     */
    @RequestMapping(value = "/error")
    @ResponseBody
    public String error(Sysuser sysusers) {
        return "请先登陆............";
    }


    /**
     * 获取用户
     *
     * @param sysusers
     * @return
     */
    @RequestMapping(value = "/getUser")
    @ResponseBody
    //  @Cacheable(value ="userCache",key ="#sysusers.name")
    public String getUser(Sysuser sysusers) {
        //代理的情况下需要用接口接收
        LogService bind = (LogService) new CacheLockDynaProxy().bind(new logServiceImpl());
        bind.getUser("1");
        Sysuser user = null;
        if (!StringEmpty.IsEmpty(sysusers.getName())) {
            user = logService.getUser(sysusers.getName());
            logger.info("从数据库查询");
            new HashMap<>();
        }
        return JSON.toJSON(user).toString();
    }

    @RequestMapping("uid")
    @ResponseBody
    public String uid(HttpSession session) {
        UUID uid = (UUID) session.getAttribute("uid");
        if (uid == null) {
            uid = UUID.randomUUID();
        }
        session.setAttribute("uid", uid);
        return session.getId();
    }
}
