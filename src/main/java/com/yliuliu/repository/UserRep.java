package com.yliuliu.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yliuliu.model.Sysuser;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 2019/6/14.
 */
public class UserRep {
    private static final Map<String, Sysuser> users = new HashMap<>();
    private static Logger logger = LoggerFactory.getLogger(UserRep.class);

    static {
        logger.info("用户初始开始");
        users.put("lyj", Sysuser.builder().ID("1").name("lyj").password("123").address("wl2002").build());
        users.put("hyy", Sysuser.builder().ID("2").name("hyy").password("123").address("wl2001").build());
        logger.info("用户初始化成功");
    }

    public static Sysuser getUser(String userId) {
        return users.get(userId);
    }

    public static void setUser(Sysuser user) {
        users.put(user.getID(), user);
    }

}
