package com.yliuliu.redisLock;

import java.lang.annotation.*;

/**
 * Created by admin on 2019/7/24.
 */
//cachelock是方法级的注解，用于注解会产生并发问题的方法
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheLock {
    //锁前缀
    String lockPrefix() default "";

    //轮询锁时间
    long timeOut() default 1000L;

    //超时时间
    long expireTime() default 3L;
}
