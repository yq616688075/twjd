package com.yliuliu.redisLock;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * Created by admin on 2019/7/23.
 */
//实现InvocationHandler进行动态代理,获取注解中的参数和方法在执行有此注解的方法前加锁,执行完成后解锁
@Component
public class CacheLockDynaProxy implements InvocationHandler {
    private Object object;
    private Logger logger = LoggerFactory.getLogger(CacheLockDynaProxy.class);

    public Object bind(Object object) {
       // System.out.println("启动代理方法");
        this.object = object;
        return Proxy.newProxyInstance(this.object.getClass().getClassLoader(), this.object.getClass().getInterfaces(), this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //logger.info("进入动态代理执行中------------------");
        //判断方法是否有加锁注解
        CacheLock cacheLock = method.getAnnotation(CacheLock.class);
        //不用加锁,通过
        if (null == cacheLock) {
            logger.info("no cachelock annotation");
            method.invoke(object, args);
        }

        //获得方法中参数的注解
        Annotation[][] parameterAnnotatins = method.getParameterAnnotations();
        String value = UUID.randomUUID().toString();

        //有LockComplexObject或者LockObject注解且参数不为空怎获取加锁参数,否则生成uuid
        if ((null != parameterAnnotatins || parameterAnnotatins.length != 0) && (null != args || args.length != 0)) {
            //有参数加锁
            Object lockedObject = getLockedObject(parameterAnnotatins, args);//根据获取到的加锁注解和参数注解获得加锁参数
            String lockedKey = lockedObject.toString();
            //开始加锁
            boolean redisLock = RedisLock.getRedisLock(lockedKey, value, cacheLock.expireTime());
            if (!redisLock){
                return null;
               // System.out.println("锁加载失败,当前线程"+Thread.currentThread().getName());
            }
            try {
                return method.invoke(object, args);
            } finally {
                //解锁
                RedisLock.releaseLock(lockedKey, value);
            }
        } else {
            //无参数加锁,使用方法名称为key加锁
            String lockedKey = method.getName();
            RedisLock.getRedisLock(lockedKey, value, cacheLock.expireTime());
            //加锁
            try {
                return method.invoke(object, args);
            } finally {
                //解锁
                RedisLock.releaseLock(lockedKey, value);
            }
        }
    }

    /**
     * 获取加锁参数(不支持多参数加锁,只支持第一个注解为lockedObject或者lockedComplexObject的参数)
     *
     * @param annotationss
     * @param args
     * @return 有标注参数则返回参数, 无参数则返回空
     */
    private Object getLockedObject(Annotation[][] annotationss, Object[] args) {
//        //判断参数是否有注解
//        if (null == annotationss || annotationss.length == 0) {
//            return new CachedLockException("没有被注解的参数");
//        }
//
//        //方法参数为空,没有锁定的对象
//        if (null == args || args.length == 0) {
//            return new CachedLockException("方法参数为空,有锁定的对象");
//        }
        //获取参数值
        for (int i = 0; i < annotationss.length; i++) {
            for (int j = 0; j < annotationss[i].length; j++) {
                //判断是否是LockComplexObject
                if (annotationss[i][j] instanceof LockComplexObject) {
                    try {
                        return args[i].getClass().getField(((LockComplexObject) annotationss[i][j]).filed());
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                }
                //判断是否是LockObject
                if (annotationss[i][j] instanceof LockObject) {
                    return args[i];
                }
            }
        }
        return null;
    }
}
