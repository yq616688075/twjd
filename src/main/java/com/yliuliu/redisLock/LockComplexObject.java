package com.yliuliu.redisLock;

import java.lang.annotation.*;

/**
 * Created by admin on 2019/7/26.
 */
//也是参数级的注解，用于注解自定义类型的参数(传入的是对象的时候使用)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LockComplexObject {
    String filed() default ""; //含有成员变量的复杂对象中需要加锁的成员变量，如一个商品对象的商品ID
}
