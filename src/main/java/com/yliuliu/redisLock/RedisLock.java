package com.yliuliu.redisLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * Created by admin on 2019/7/28.
 */
@Component
public class RedisLock {

    private static RedisTemplate redisTemplate;

    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        RedisLock.redisTemplate = redisTemplate;
    }

    //锁是否成功
    private static final Long LOCK_SUCCESS_OF_SPRINGREDISTEMPLATE = 1L;


    /**
     * 获取锁
     *
     * @param key
     * @param value
     * @param expireTime 超时时间/单位-秒
     * @return
     */
    public static boolean getRedisLock(String key, String value, long expireTime) {
        try {
            String script = "if redis.call('setNx',KEYS[1],ARGV[1]) then if redis.call('get',KEYS[1])==ARGV[1] then return redis.call('expire',KEYS[1],ARGV[2]) else return 0 end end";
            RedisScript<String> redisScript = new DefaultRedisScript<>(script, String.class);
            Object execute = redisTemplate.execute(redisScript, Collections.singletonList(key), value, expireTime);
            if (LOCK_SUCCESS_OF_SPRINGREDISTEMPLATE.equals(execute)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 解锁
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean releaseLock(String key, String value) {
        //这里为什么需要用key=value然后删除锁,原因:只能是谁加的锁谁解锁,使用lua操作,保证原子性
       // System.out.println("解锁了---");
        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        RedisScript<String> redisScript = new DefaultRedisScript<>(script, String.class);
        Object execute = redisTemplate.execute(redisScript, Collections.singletonList(key), value);
        if (LOCK_SUCCESS_OF_SPRINGREDISTEMPLATE.equals(execute)) {
            return true;
        }
        return false;
    }

    //----------------------------------使用jedis实现-------------------------------------------------
    //private static final String LOCK_SUCCESS_OF_JEDIS="OK";
    //设置key如果不存在则设置,否责不设置
    //private static final String SET_IF_NOT_EXIST="NX";
    //设置超时时间
    //private static final String SET_WITH_EXPIRE_TIME="PX";
    //加锁实现 jedis.set(lockKey, requestId, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime)
    //解锁实现
    //String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
    //Object result = jedis.eval(script, Collections.singletonList(lockKey), Collections.singletonList(requestId));
}
