package com.yliuliu.redisLock;

import java.lang.annotation.*;

/**
 * Created by admin on 2019/7/26.
 */
//是参数级的注解，用于注解商品ID等基本类型的参数
@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface LockObject {
    //不需要参数
}
