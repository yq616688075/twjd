package com.yliuliu.common.base;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.Key;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
public class DESCoder {
    /**
     * 密钥算法
     * java支持56位密钥，bouncycastle支持64位
     */
    public static final String KEY_ALGORITHM = "DES";

    private static final byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};

    /**
     * 加密/解密算法/工作模式/填充方式
     * DES/CBC/PKCS5Padding
     */
    public static final String CIPHER_ALGORITHM_CBC_PKCS5PADDING = "DES/CBC/PKCS5Padding";

    /**
     * 加密/解密算法/工作模式/填充方式
     * DES
     * 相当于
     * DES/ECB/PKCS7Padding
     */
    public static final String CIPHER_ALGORITHM_DES = "DES";

    /**
     * 生成密钥，java6只支持56位密钥，bouncycastle支持64位密钥
     *
     * @return byte[] 二进制密钥
     */
    public static byte[] initkey() throws Exception {

        //实例化密钥生成器
        KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM);
        //初始化密钥生成器
        kg.init(56);
        //生成密钥
        SecretKey secretKey = kg.generateKey();
        //获取二进制密钥编码形式
        return secretKey.getEncoded();
    }

    /**
     * 转换密钥
     *
     * @param key 二进制密钥
     * @return Key 密钥
     */
    public static Key toKey(byte[] key) throws Exception {
        //实例化Des密钥
        DESKeySpec dks = new DESKeySpec(key);
        //实例化密钥工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(KEY_ALGORITHM);
        //生成密钥
        SecretKey secretKey = keyFactory.generateSecret(dks);
        return secretKey;
    }

    /**
     * 加密数据
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密后的数据
     */
    public static byte[] encrypt(byte[] data, byte[] key) throws Exception {
        return encrypt(data, key, CIPHER_ALGORITHM_DES);
    }

    /**
     * 加密数据
     *
     * @param data 待加密数据
     * @param key  密钥
     * @param cipherAlgorithm 加密模式
     * @return byte[] 加密后的数据
     */
    public static byte[] encrypt(byte[] data, byte[] key, String cipherAlgorithm) throws Exception {
        //还原密钥
        Key k = toKey(key);
        //实例化
        if (CIPHER_ALGORITHM_CBC_PKCS5PADDING.equals(cipherAlgorithm)) {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            IvParameterSpec ips = new IvParameterSpec(iv);
            //初始化，设置为加密模式
            cipher.init(Cipher.ENCRYPT_MODE, k, ips);
            //执行操作
            return cipher.doFinal(data);
        } else if (CIPHER_ALGORITHM_DES.equals(cipherAlgorithm)) {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            //初始化，设置为加密模式
            cipher.init(Cipher.ENCRYPT_MODE, k);
            //执行操作
            return cipher.doFinal(data);
        }
        throw new RuntimeException("不支持的加解密模式");
    }

    /**
     * 解密数据
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密后的数据
     */
    public static byte[] decrypt(byte[] data, byte[] key) throws Exception {
        return decrypt(data, key, CIPHER_ALGORITHM_DES);
    }

    /**
     * 解密数据
     *
     * @param data 待解密数据
     * @param key  密钥
     * @param cipherAlgorithm 加密模式
     * @return byte[] 解密后的数据
     */
    public static byte[] decrypt(byte[] data, byte[] key, String cipherAlgorithm) throws Exception {
        //欢迎密钥
        Key k = toKey(key);
        //实例化
        if (CIPHER_ALGORITHM_CBC_PKCS5PADDING.equals(cipherAlgorithm)) {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            IvParameterSpec ips = new IvParameterSpec(iv);
            //初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, k, ips);
            //执行操作
            return cipher.doFinal(data);
        } else if (CIPHER_ALGORITHM_DES.equals(cipherAlgorithm)) {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            //初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, k);
            //执行操作
            return cipher.doFinal(data);
        }
        throw new RuntimeException("不支持的加解密模式");
    }

}