package com.yliuliu.common.base;

import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/**
 * Created by CHENHM8 on 2017-10-24.
 */
public class Constant {

    // 禁止使用，没有中间件化，导致接入每一个适配器，这个值类型都不一样，或，接入客户单点时，类型也不一样
    @Deprecated
    public final static String SAAS_TOKEN_USER_INFO = "userInfo";

    public final static String SAAS_TOKEN_USER_INFO_JSON = "userInfojson";

    public final static String SAAS_TOKEN_USER_ID = "userId";

    public final static String SAAS_TOKEN_USER_NAME = "userName";

    public final static String SAAS_CRM_CUSTOMER = "crm_customer";

    public final static String SAAS_CRM_PRODUCT = "crm_product";

    public final static String SAAS_CRM_LEAD = "crm_lead";

    public final static String SAAS_CRM_BUSINESS = "crm_business";

    public final static String SAAS_CRM_COMPETITION = "crm_competition";

    public final static String SAAS_CRM_ADMIN_YES = "Y";

    public final static String SAAS_CRM_PROCESS_TENANTID="143616744011111111";

    public static String SAAS_MUC_SOURCE_ID = "";//"e21c13e0-b57a-49f1-985b-88888888888";

    public final static String QUOTATION_PROJECT_MANAGER = "N32";

    public final static Integer UPDATE_OR_INSERT = 0;
    public final static Integer DELETE = 1;

    static{
        if(StringUtils.isBlank(SAAS_MUC_SOURCE_ID)){
            Properties properties = BaseProperty.loadProps("config/config.properties");
            SAAS_MUC_SOURCE_ID = properties.getProperty("muc.resource.id");
        }
    }

}
