package com.yliuliu.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 行命令工具
 * @author liangjj15
 *
 */
public class CmdUtil {
	private static Logger logger = LoggerFactory.getLogger(CmdUtil.class);

    /**
	 * 执行行命令
	 * @param cmd
	 * @return
	 * @throws Exception
     */
	public static int exec(String cmd) throws Exception{
		Process process = null;
		try {
			Runtime runtime = Runtime.getRuntime();
			process = runtime.exec(cmd);
			InputStream errorStream = process.getErrorStream();
			InputStream inputStream = process.getInputStream();
			cleanStream(errorStream);
			cleanStream(inputStream);
			int result = process.waitFor();
			return result;
		} finally{
			if(null!=process){
				try {
					process.destroy();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}

	/**
	 * 清除流
	 * @param inputStream
	 */
	private static void cleanStream(final InputStream inputStream){
		if(null==inputStream) return ;
		Thread thread = new Thread(new Runnable() {
			public void run() {
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(inputStream));
					String line = null;
					//logger.info("cleanStream");
					while((line=reader.readLine())!=null){
						//logger.info(line);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally{
					try {
						inputStream.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						if(null!=reader){
							reader.close();
						}
					} catch (Exception e2) {
					}
				}
			}
		});
		thread.start();
	}
}
