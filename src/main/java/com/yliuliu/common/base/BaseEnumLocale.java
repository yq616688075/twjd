package com.yliuliu.common.base;

import java.io.Serializable;

/**
 * 枚举基本接口
 * createTime 2017-08-23 16:08
 *
 * @author 陆国鸿
 */
public interface BaseEnumLocale<C> extends BaseEnum<C>, Serializable {
    BaseEnum<String> localeItem();
}
