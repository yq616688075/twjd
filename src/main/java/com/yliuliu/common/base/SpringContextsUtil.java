package com.yliuliu.common.base;


/**
 * Created by BAIBG1 on 2017-4-20.
 */
public final class SpringContextsUtil {

    private SpringContextsUtil() {
    }

    public static <T> T getBean(Class<T> clz) {
        return SpringContextHolder.getBean(clz);
    }

    public static Object getBean(String name) {
        return SpringContextHolder.getBean(name);
    }

}
