package com.yliuliu.common.base;

//
//import com.baomidou.mybatisplus.toolkit.IdWorker;
//import com.meicloud.saas.data.RedisDataManager;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.Random;

public final class SerialUtil {

//    private SerialUtil() {}
//
//    public static String getNextId() {
//        return makeSerialId(20);
//    }

//    public static long getGeneratorId() {
//        return IdWorker.getId();
//    }

//
//    public static String makeSerialId(int maxLength) {
//
//        return makeSerialId(null, maxLength);
//    }

//    public static String makeSerialId(String prefix, int maxLength) {
//        return makeSerialId(prefix, maxLength, true);
//    }

//    public static String makeSerialId(String prefix, int maxLength, boolean insertDatetime) {
//        StringBuffer buffer = new StringBuffer();
//        int length = maxLength;
//        if (StringUtil.isPresent(prefix)) {
//            buffer.append(prefix);
//            length -= prefix.length();
//        }
//        if (insertDatetime) {
//            String datetime = DateFormatUtils.format(new Date(), "yyMMddHHmmss");
//            buffer.append(datetime);
//            length -= datetime.length();
//        }
//        String serial = formatNumber(getNextSerial(), length);
//        buffer.append(serial);
//
//        return buffer.toString();
//    }

    private static String formatNumber(long value, int maxLength) {
        String valueStr = Long.valueOf(value).toString();
        if (valueStr.length() < maxLength) {
            int index = maxLength - valueStr.length();
            StringBuffer buffer = new StringBuffer();
            while (index > 0) {
                buffer.append("0");
                index -= 1;
            }
            buffer.append(valueStr);
            return buffer.toString();
        }
        return valueStr.substring(valueStr.length() - maxLength, valueStr.length());
    }

//    private static long getNextSerial() {
//        RedisDataManager manager =  SpringContextsUtil.getBean(RedisDataManager.class);
//        if (manager != null) {
//            return manager.incr("serial_id");
//        }
//        return new Random().nextLong();
//    }
}
