package com.yliuliu.common.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MobileUtil {

    private MobileUtil() {
    }

    private static final String MOBILE_REGEXP = "^1(3\\d|4[57]|5[^4\\D]|7\\d|8\\d)\\d{8}$";

    public static boolean check(String mobile) {
        if (mobile != null) {
            Pattern p = Pattern.compile(MOBILE_REGEXP);
            Matcher m = p.matcher(mobile);
            return m.matches();
        }
        return false;
    }
}
