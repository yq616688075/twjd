package com.yliuliu.common.base;

/**
 * Created by CHENMK3 on 2018-7-5
 */
public enum StatusEnum {

    VALID(1,"有效"),
    INVALID(0,"无效");

    public final Integer value;
    public final String name;

    StatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public static StatusEnum getEnumByName(String name) {
        if(name.equals("有效")){
            return StatusEnum.VALID;
        }else if (name.equals("无效")){
            return StatusEnum.INVALID;
        }
        return StatusEnum.INVALID;
    }

    public static StatusEnum getEnumByValue(Integer value) {
        if(value==1){
            return StatusEnum.VALID;
        }else if (value==0){
            return StatusEnum.INVALID;
        }
        return StatusEnum.INVALID;
    }
}
