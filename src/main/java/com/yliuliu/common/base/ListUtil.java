package com.yliuliu.common.base;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
public final class ListUtil {
    private ListUtil() {}

    public static boolean isPresent(Collection list) {
        return list != null && !list.isEmpty();
    }

    public static boolean isBlank(Collection list) {
        return list == null || list.isEmpty();
    }

    public static String join(Collection list, String sep) {
        if (isBlank(list)) {
            return "";
        }

        String prefix = "";
        StringBuffer buffer = new StringBuffer();
        for(Iterator<Object> iter = list.iterator(); iter.hasNext(); prefix = sep) {
            buffer.append(prefix).append(iter.next());
        }
        return buffer.toString();
    }

    public static <T> List<T> map(Collection list, String field) {
        List<T> result = new ArrayList<T>();
        try {
            String methodName = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            for (Object value : list) {
                Method method = value.getClass().getMethod(methodName);
                T fieldValue = (T) method.invoke(value);
                result.add(fieldValue);
            }
        }
        catch (Exception e) {
            throw new RuntimeException("对象没有定义" + field);
        }
        return result;
    }

    public static <T, E> List<T> map(Collection<E> list, IteratorTask<T, E> runnable) {
        List<T> result;
//        if (list instanceof Page) {
//            result = new Page<>();
//            BeanUtils.copyProperties(list, result);
//        }
//        else {
            result = new ArrayList<>(list.size());
//        }

        for (E item : list) {
            result.add(runnable.getValue(item));
        }
        return result;
    }

    public interface IteratorTask<TargetType, SourceType> {
        TargetType getValue(SourceType item);
    }

    /**
     * 清除List中null值
     * */
    public static <T> List<T> removeNull(List<? extends T> oldList) {
        List<T> listTemp = new ArrayList();
        for (int i = 0;i < oldList.size(); i++) {
            if (oldList.get(i) != null) {
                listTemp.add(oldList.get(i));
            }
        }
        return listTemp;
    }


    /**
     * 按指定大小，分隔集合，将集合按规定个数分为n个部分
     * @param list
     * @param len
     * @return
     */
    public static <T> List<List<T>> splitList(List<T> list, int len) {
        if (list == null || list.size() == 0 || len < 1) {
            return null;
        }
        List<List<T>> result = new ArrayList<List<T>>();
        int size = list.size();
        int count = (size + len - 1) / len;
        for (int i = 0; i < count; i++) {
            List<T> subList = list.subList(i * len, ((i + 1) * len > size ? size : len * (i + 1)));
            result.add(subList);
        }
        return result;
    }

    /**
     * list 集合分组
     *
     * @param list    待分组集合
     * @param groupBy 分组Key算法
     * @param <K>     分组Key类型
     * @param <V>     行数据类型
     * @return 分组后的Map集合
     */
    public static <K, V> Map<K, List<V>> groupBy(List<V> list, GroupBy<K, V> groupBy) {
        return groupBy((Collection<V>) list, groupBy);
    }
    /**
     * list 集合分组
     *
     * @param list    待分组集合
     * @param groupBy 分组Key算法
     * @param <K>     分组Key类型
     * @param <V>     行数据类型
     * @return 分组后的Map集合
     */
    public static <K, V> Map<K, List<V>> groupBy(Collection<V> list, GroupBy<K, V> groupBy) {
        Map<K, List<V>> resultMap = new LinkedHashMap<K, List<V>>();
        for (V e : list) {
            K k = groupBy.groupBy(e);
            if (resultMap.containsKey(k)) {
                resultMap.get(k).add(e);
            } else {
                List<V> tmp = new LinkedList<V>();
                tmp.add(e);
                resultMap.put(k, tmp);
            }
        }
        return resultMap;
    }
    /**
     * List分组
     *
     * @param <K> 返回分组Key
     * @param <V> 分组行
     */
    public interface GroupBy<K, V> {
        K groupBy(V row);
    }

    public static <S,D> List<D> copyProperties(List<S> sources,Class clazz) {
        List<D> targets = JSON.parseArray(JSON.toJSONString(sources),clazz);
        return targets;
    }
}
