package com.yliuliu.common.base;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * Created by BAIBG1 on 2016-7-14.
 */
public final class StatisticUtil {

    private StatisticUtil() {}

    public static final String FULL_DATE_TIME_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_TIME_FORMAT = "yyyyMMddHH";
    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String MONTH_FORMAT = "yyyyMM";

    public static Integer getCurrentDateTime() {
        return getValue(new Date(), DATE_TIME_FORMAT);
    }

    public static Integer getCurrentDate() {
        return getValue(new Date(), DATE_FORMAT);
    }

    public static Integer getValue(Date date, String format) {
        String value = DateFormatUtils.format(date, format);
        return Integer.valueOf(value);
    }

    public static Integer getDate(Date date) {
        String value = DateFormatUtils.format(date, DATE_FORMAT);
        return Integer.valueOf(value);
    }

    public static Integer getYearMonth(Date date) {
        return getValue(date, MONTH_FORMAT);
    }

    public static Integer getYearMonthDay(Date date) {
        return getValue(date, DATE_FORMAT);
    }

//    public static Date parseDateTime(Integer datetime) throws ParseException {
//        return parseDateTime(datetime, DATE_TIME_FORMAT);
//    }
//
//    public static Date parseDateTime(Integer datetime, String format) throws ParseException{
//        DateFormat df = new SimpleDateFormat(format);
//        return df.parse(datetime.toString());
//    }
}
