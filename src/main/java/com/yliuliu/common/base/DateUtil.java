package com.yliuliu.common.base;


import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by baibg1 on 2016-7-20.
 */
public final class DateUtil {
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_INTEGER_PATTERN = "yyyyMMdd";
    public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_PATTERN = "HH:mm:ss";
    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private DateUtil() {
    }


    public static Date getBeginningOfDay(Date date) {
        date = DateUtils.setHours(date, 0);
        date = DateUtils.setMinutes(date, 0);
        date = DateUtils.setSeconds(date, 0);
        date = DateUtils.setMilliseconds(date, 0);
        return date;
    }

    public static Date getEndingOfDay(Date date) {
        date = DateUtils.setHours(date, 23);
        date = DateUtils.setMinutes(date, 59);
        date = DateUtils.setSeconds(date, 59);
        date = DateUtils.setMilliseconds(date, 999);
        return date;
    }

    public static Date getBeginningOfWeek(Date date) {
        date = DateUtils.addDays(date, -1 * date.getDay());
        return getBeginningOfDay(date);
    }

    public static Date getEndingOfWeek(Date date) {
        date = DateUtils.addDays(date, -1 * date.getDay() + 6);
        return getEndingOfDay(date);
    }

    public static Date getBeginningOfMonth(Date date) {
        date = DateUtils.setDays(date, 1);
        return getBeginningOfDay(date);
    }

    public static Date getEndingOfMonth(Date date) {
        date = getBeginningOfMonth(date);
        date = DateUtils.addMonths(date, 1);
        date = DateUtils.addDays(date, -1);
        return getEndingOfDay(date);
    }

    /**
     * 获取指定年月的开始时间
     * @param year
     * @param month
     * @return
     */
    public static Date getBeginningOfMonth(Integer year, Integer month) {
        Calendar time = Calendar.getInstance();
        time.set(year.intValue(),month.intValue() - 1,1,0,0,0);
        return time.getTime();
    }

    /**
     * 获取指定年月的开始时间
     * @param year
     * @param month
     * @return
     */
    public static String getBeginningOfMonth(Integer year, Integer month, String format) {
        Calendar time = Calendar.getInstance();
        time.set(year.intValue(),month.intValue() - 1,1,0,0,0);
        return format(time.getTime(), format);
    }

    /**
     * 获取指定年月的结束时间
     * @param year
     * @param month
     * @return
     */
    public static Date getEndingOfMonth(Integer year, Integer month) {
        Calendar time = Calendar.getInstance();
        time.set(year.intValue(),month.intValue(),0,23,59,59);
        return time.getTime();
    }

    /**
     * 获取指定年月的结束时间
     * @param year
     * @param month
     * @return
     */
    public static String getEndingOfMonth(Integer year, Integer month, String format) {
        Calendar time = Calendar.getInstance();
        time.set(year.intValue(),month.intValue(),0,23,59,59);
        return format(time.getTime(), format);
    }

    /**
     * 获取当年的第一天
     * @return
     */
    public static Date getBeginingOfYear(){
        Calendar currCal= Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        return getYearFirst(currentYear);
    }

    /**
     * 获取当年的最后一天
     * @return
     */
    public static Date getEndingOfYear(){
        Calendar currCal= Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        return getYearLast(currentYear);
    }

    /**
     * 获取某年第一天日期
     * @param year 年份
     * @return Date
     */
    public static Date getYearFirst(int year){
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        return currYearFirst;
    }

    /**
     * 获取某年最后一天日期
     * @param year 年份
     * @return Date
     */
    public static Date getYearLast(int year){
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();

        return currYearLast;
    }


    public static boolean isBetween(Date source, Date date1, Date date2) {
        if (source == null && date1 == null && date2 == null) {
            return true;
        }

        if (source == null || date1 == null || date2 == null) {
            return false;
        }
        return source.compareTo(date1) >= 0 && source.compareTo(date2) <= 0;
    }

    public static Date addMonth(Date date, int amount) {
        return DateUtils.addMonths(date, amount);
    }

    public static Date addYear(Date date, int amount) {
        return DateUtils.addYears(date, amount);
    }

    public static Date addDay(Date date, int amount) {
        return DateUtils.addDays(date, amount);
    }

    public static Date addHour(Date date, int amount) {
        return DateUtils.addHours(date, amount);
    }
    public static Date addMinutes(Date date, int amount) {
        return DateUtils.addMinutes(date, amount);
    }

    /**
     * 获取当前时间
     * <p>
     * createTime 2017-8-23 15:53
     *
     * @return Date
     * @author 陆国鸿
     */
    public static Date getCurrentDate() {
        return getCurrentDate(true);
    }

    /**
     * 
     * 创建时间 2018-8-9 14:09
     * @author 陆国鸿
     * @param needTime 是否需要时间部分
     * @return
     */
    public static Date getCurrentDate(boolean needTime) {
        Calendar now = Calendar.getInstance();
        if (needTime) {
            return now.getTime();
        } else {
            now.set(Calendar.HOUR_OF_DAY, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            return now.getTime();
        }
    }

    /**
     * 字符串转日期
     * 创建时间 2017-8-25 18:14
     *
     * @param dateStr
     * @param pattern
     * @return
     * @author 陆国鸿
     */
    public static Date parseDate(String dateStr, String pattern) {
        try {
            return DateUtils.parseDate(dateStr, pattern);
        } catch (ParseException e) {
            throw new IllegalArgumentException("dateStr is not fitted for pattern");
        }
    }

    public static Date parseDate(String dateStr) {
        SimpleDateFormat format = null;
        if (StringUtil.isBlank(dateStr))
            return null;
        String _dateStr = dateStr.trim();
        try {
            if (_dateStr.matches("\\d{1,2}[A-Z]{3}")) {
                _dateStr = _dateStr + (Calendar.getInstance().get(Calendar.YEAR) - 2000);
            }
            // 01OCT12
            if (_dateStr.matches("\\d{1,2}[A-Z]{3}\\d{2}")) {
                format = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
            } else if (_dateStr.matches("\\d{1,2}[A-Z]{3}\\d{4}.*")) {// 01OCT2012
                // ,01OCT2012
                // 1224,01OCT2012
                // 12:24
                _dateStr = _dateStr.replaceAll("[^0-9A-Z]", "").concat("000000").substring(0, 15);
                format = new SimpleDateFormat("ddMMMyyyyHHmmss", Locale.ENGLISH);
            } else {
                StringBuffer sb = new StringBuffer(_dateStr);
                String[] tempArr = _dateStr.split("\\s+");
                tempArr = tempArr[0].split("-|\\/");
                if (tempArr.length == 3) {
                    if (tempArr[1].length() == 1) {
                        sb.insert(5, "0");
                    }
                    if (tempArr[2].length() == 1) {
                        sb.insert(8, "0");
                    }
                }
                _dateStr = sb.append("000000").toString().replaceAll("[^0-9]", "").substring(0, 14);
                if (_dateStr.matches("\\d{14}")) {
                    format = new SimpleDateFormat("yyyyMMddHHmmss");
                }
            }

            if (format == null) {
                return null;
            }
            Date date = format.parse(_dateStr);
            return date;
        } catch (Exception e) {
            throw new RuntimeException("无法解析日期字符[" + dateStr + "]");
        }
    }

    public static String format(Date date, String... patterns) {
        if (date == null)
            return "";
        String pattern = TIMESTAMP_PATTERN;
        if (patterns != null && patterns.length > 0 && !StringUtil.isBlank(patterns[0])) {
            pattern = patterns[0];
        }
        return DateFormatUtils.format(date, pattern);
    }

    /**
     * 把日期部分格式化成整型
     *
     * @param date 日期
     * @return Integer
     */
    public static Integer formatDate2Integer(Date date) {
        String dateString = format(date, DATE_INTEGER_PATTERN);
        Integer result = Integer.parseInt(dateString);
        return result;
    }

    public static Date getSaasCreateDate() {
        SimpleDateFormat beferDateFormat = new SimpleDateFormat(DATE_PATTERN);
        Date saasDate = null;
        try {
            saasDate = beferDateFormat.parse("2017-09-13");
        } catch (ParseException e) {
            logger.error("", e);
        }
        return saasDate;
    }

    public static Date getDate(String type, String strDate) {
        SimpleDateFormat beferDateFormat = new SimpleDateFormat(type);
        Date date = null;
        try {
            date = beferDateFormat.parse(strDate);
        } catch (ParseException e) {
            logger.error("", e);
        }
        return date;
    }

    public static Map<String, Date> convertMonthByDate(Date date) {
        Map<String, Date> map = new HashMap<String, Date>();
        // 指定日期月第一天
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.DATE, 1);
        Date theDate = calendar.getTime();
        // 指定日期月最后一天
        calendar.add(Calendar.MONTH, 1); // 加一个月
        calendar.set(Calendar.DATE, 1); // 设置为该月第一天
        calendar.add(Calendar.DATE, -1); // 再减一天即为上个月最后一天
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        map.put("first", theDate);
        map.put("last", calendar.getTime());
        return map;
    }

    public static List<String> getMonthBetween(String minDate, String maxDate) {
        return getMonthBetween(minDate,maxDate,"yyyy-MM","yyyy-MM");
    }

    public static List<String> getMonthBetween(String minDate, String maxDate, String inFormat, String outFormat) {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat inSdf = new SimpleDateFormat(inFormat);//格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        try {
            min.setTime(inSdf.parse(minDate));
            min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

            max.setTime(inSdf.parse(maxDate));
            max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        Calendar curr = min;
        SimpleDateFormat outSdf = new SimpleDateFormat(outFormat);//格式化为年月
        while (curr.before(max)) {
            result.add(outSdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    public static List<String> getDayBetween(String minDate, String maxDate){
        return getDayBetween(minDate,maxDate,DATE_PATTERN,DATE_PATTERN);
    }

    public static List<String> getDayBetween(String minDate, String maxDate, String inFormat, String outFormat) {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat inSdf = new SimpleDateFormat(inFormat);//格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        try {
            min.setTime(inSdf.parse(minDate));
            min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH),min.get(Calendar.DATE), 0,0,0);

            max.setTime(inSdf.parse(maxDate));
            max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH),max.get(Calendar.DATE), 23,59,59);

        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        Calendar curr = min;
        SimpleDateFormat outSdf = new SimpleDateFormat(outFormat);//格式化为年月
        while (curr.before(max)) {
            result.add(outSdf.format(curr.getTime()));
            curr.add(Calendar.DATE, 1);
        }

        return result;
    }

    /***
    * @Description: 通过时间秒毫秒数判断两个时间的间隔
    * @Param: [date1, date2]
    * @return: int
    * @Author: CHENMK3
    * @Date: 2018-10-10
    */
    public static int differentDaysByMillisecond(Date date1, Date date2){
        int days = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
        return days;
    }

    /*** 
    * @Description: 通过时间秒毫秒数判断两个日期的间隔
    * @Param: [date1, date2]
    * @return: int 
    * @Author: CHENMK3
    * @Date: 2018-10-10 
    */ 
    public static int differentDaysByDay(Date date1, Date date2){
        return differentDaysByMillisecond(DateUtil.getBeginningOfDay(date1), DateUtil.getBeginningOfDay(date2));
    }

    /*** 
    * @Description: 获取当前日期的前一天
    * @Param: [date]
    * @return: java.utils.Date
    * @Author: CHENMK3
    * @Date: 2018-10-10 
    */ 
    public static Date getTheDayBefore(Date date){
        Date nowDate = getBeginningOfDay(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        return calendar.getTime();
    }

    /*** 
    * @Description: 根据日期获取对应年份月份的月的天数
    * @Param: [date]
    * @return: int 
    * @Author: CHENMK3
    * @Date: 2018-10-10 
    */ 
    public static int getDaysByDate(Date date){
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        int year = dateCalendar.get(Calendar.YEAR);
        int month = dateCalendar.get(Calendar.MONTH);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DATE, 1);
        calendar.roll(Calendar.DATE, -1);
        int maxDate = calendar.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取过去或者未来 任意天内的日期数组
     * @param intervals      intervals天内
     * @return              日期数组
     */
    public static ArrayList<Date> test(int intervals ) {
        ArrayList<Date> pastDaysList = new ArrayList<>();
        ArrayList<Date> fetureDaysList = new ArrayList<>();
        for (int i = 0; i <intervals; i++) {
            pastDaysList.add(getPastDate(i));
            fetureDaysList.add(getFutureDate(i));
        }
        return pastDaysList;
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static Date getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        return today;
    }

    /**
     * 获取未来 第 past 天的日期
     * @param past
     * @return
     */
    public static Date getFutureDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);
        Date today = calendar.getTime();
        return today;
    }

    public static Date getDateManyMintueAfterNow(int minutes){
        Calendar nowBefore = Calendar.getInstance();
        nowBefore.add(Calendar.MINUTE, minutes);
        return nowBefore.getTime();
    }

    /**
     *
     * @return
     */
    public static String getCurrentTimeStr(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date();
        return sdf.format(date);
    }


    /**
    * 指定日期加上天数后的日期
    * @throws ParseException
    */
     public static Date plusDay(int num,Date curDate) {
         Calendar ca = Calendar.getInstance();
         ca.setTime(curDate);
         ca.add(Calendar.DATE, num);// num为增加的天数，可以改变的
         return ca.getTime();
     }

    /**
     * 获取星期几
     * 0：周日 1：周一 2：周二 3：周三 4：周四 5：周五 6：周六
     * @param date
     * @return
     */
    public static int getDayOfWeek(Date date) {
        if (null == date) {
            throw new RuntimeException("date can not be null");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 获取星期几
     * 0：周日 1：周一 2：周二 3：周三 4：周四 5：周五 6：周六
     * @param date
     * @return
     */
    public static int getDayOfMonth(Date date) {
        if (null == date) {
            throw new RuntimeException("date can not be null");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

}
