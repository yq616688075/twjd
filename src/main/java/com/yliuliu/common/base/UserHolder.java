package com.yliuliu.common.base;

/**
 * Created by BAIBG1 on 2018-8-30.
 */
public final class UserHolder {

    private static ThreadLocal<String> sourceIdHolder = new ThreadLocal<>();

    private static ThreadLocal<String> uidHolder = new ThreadLocal<>();

    /**
     * 用户是否校验标记，可能为null
     */
    private static ThreadLocal<Boolean> userCheckPassHolder = new ThreadLocal<>();

    /**
     * 用户校验方式
     * 程序自定义
     */
    private static ThreadLocal<String> userCheckPassTypeHolder = new ThreadLocal<>();

    public static void setUid(String uid) {
        setOrRemove(uidHolder, uid);
    }

    public static String getUid() {
        return uidHolder.get();
    }

    public static void setSourceId(String sourceId) {
        setOrRemove(sourceIdHolder, sourceId);
    }

    public static String getSourceId() {
        return sourceIdHolder.get();
    }

    public static void setUserCheckPassHolder(boolean userCheckPass) {
        userCheckPassHolder.set(userCheckPass);
    }

    public static Boolean getUserCheckPassHolder() {
        Boolean userCheckPass = userCheckPassHolder.get();
        if (null == userCheckPass) {
            return false;
        }
        return userCheckPass;
    }

    public static void setUserCheckPassTypeHolder(String userCheckPassType) {
        setOrRemove(userCheckPassTypeHolder, userCheckPassType);
    }

    public static String getUserCheckPassTypeHolder() {
        return userCheckPassTypeHolder.get();
    }

    private static <T> void  setOrRemove(ThreadLocal<T> holder, T value) {
        if (value == null) {
            holder.remove();
        }
        else {
            holder.set(value);
        }
    }


}
