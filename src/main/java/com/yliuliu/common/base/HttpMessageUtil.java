package com.yliuliu.common.base;

//import com.meicloud.paas.core.constants.ResponseCode;
//import com.meicloud.paas.core.exception.BaseException;
//import com.meicloud.saas.vo.PushData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpMessageUtil {
//    /**
//     * 日志处理类
//     */
//    private static final Log log = LogFactory.getLog(HttpMessageUtil.class);
//
//    public static Map sendMobile(PushData bean, HttpHost proxy) throws Exception {
//        String r = postData(bean, proxy);
//        Map<String, String> map = new HashMap<String, String>();
//        if (r != null) {
//            String[] reInofArr = r.split("&");
//            for (int i = 0; i < reInofArr.length; i++) {
//                String keyValue = reInofArr[i];
//                if (keyValue != null) {
//                    String[] keyValueArr = keyValue.split("=");
//                    map.put(keyValueArr[0], keyValueArr[1]);
//
//                }
//            }
//        }
//        return map;
//    }
//
//    /**
//     * 发送HTTP请求
//     *
//     * @param bean
//     * @return
//     */
//    private static String postData(PushData bean, HttpHost proxy) throws Exception {
//
//        String url = bean.getUrl();
//        HashMap<String, String> params = bean.getParams();
//        if (null == params || params.size() == 0) {
//            throw new BaseException(ResponseCode.FAIL, "无传递参数[params.size = 0]");
//        }
//        List pairs = buildNameValuePairs(params);//组装参数
//        return post(url, pairs, proxy);
//    }
//
//    private static void close(Closeable closeable) {
//        if (closeable != null) {
//            try {
//                closeable.close();
//            } catch (IOException e) {
//                log.error(e.getMessage(), e);
//            }
//        }
//    }
//
//    /**
//     * Get请求
//     *
//     * @param url
//     * @param params
//     * @return
//     */
//    public static String get(String url, List<NameValuePair> params, HttpHost proxy) throws Exception {
//        HttpGet httpget = new HttpGet(url);
//        String str = EntityUtils.toString(new UrlEncodedFormEntity(params));
//        httpget.setURI(new URI(httpget.getURI().toString() + "?" + str));
//        return sendHttpRequest(httpget, proxy);
//    }
//
//    /**
//     * Post请求
//     *
//     * @param url
//     * @param params
//     * @return
//     */
//    public static String post(String url, List<NameValuePair> params, HttpHost proxy) throws Exception {
//        // Post请求
//        HttpPost httppost = new HttpPost(url);
//        httppost.setEntity(new UrlEncodedFormEntity(params));
//        return sendHttpRequest(httppost, proxy);
//    }
//
//    private static String sendHttpRequest(HttpRequestBase httpRequest, HttpHost proxy) throws Exception {
//        CloseableHttpClient httpClient = null;
//        try {
//            httpClient = HttpClients.createDefault();
//            httpRequest.setConfig(getRequestConfig(proxy));
//            httpRequest.setHeader("Charset", "UTF-8");
//            HttpResponse httpresponse = httpClient.execute(httpRequest);
//            HttpEntity entity = httpresponse.getEntity();
//            return EntityUtils.toString(entity);
//        } catch (IOException e) {
//            throw e;
//        } finally {
//            close(httpClient);
//        }
//    }
//
//    /**
//     * 组装参数,这里用Map,一键一值比较通用,可以当做共用方法
//     *
//     * @param params
//     * @return
//     */
//    private static List<NameValuePair> buildNameValuePairs(Map<String, String> params) {
//        Object[] keys = params.keySet().toArray();
//        List pairs = new ArrayList();
//        for (int i = 0; i < keys.length; i++) {
//            String key = (String) keys[i];
//            pairs.add(new BasicNameValuePair(key, params.get(key)));
//        }
//
//        return pairs;
//    }
//
//    private static RequestConfig getRequestConfig(HttpHost proxy) {
//        int httpTimeOutTime = 3;
//        RequestConfig.Builder builder = RequestConfig.custom()
//                .setSocketTimeout(1000 * httpTimeOutTime)
//                .setConnectTimeout(1000 * httpTimeOutTime)
//                .setConnectionRequestTimeout(1000 * httpTimeOutTime);
//        if (proxy != null) {
//            builder = builder.setProxy(proxy);
//        }
//        return builder.build();
//    }
//    public static String sendPost(String url, String param) {
//        PrintWriter out = null;
//        BufferedReader in = null;
//        String result = "";
//        try {
//            URL realUrl = new URL(url);
//            // 打开和URL之间的连接
//            URLConnection conn = realUrl.openConnection();
//            // 发送POST请求必须设置如下两行
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            // 获取URLConnection对象对应的输出流
//            out = new PrintWriter(conn.getOutputStream());
//            // 发送请求参数
//            out.print(param);
//            // flush输出流的缓冲
//            out.flush();
//            // 定义BufferedReader输入流来读取URL的响应
//            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String line;
//            while ((line = in.readLine()) != null) {
//                result += line;
//            }
//        } catch (Exception e) {
//            System.out.println("发送 POST 请求出现异常！" + e);
//            log.error(e.getMessage(),e);
//        } finally {
//            try {
//                if (out != null) {
//                    out.close();
//                }
//                if (in != null) {
//                    in.close();
//                }
//            } catch (IOException ex) {
//                log.error(ex.getMessage(),ex);
//            }
//        }
//        return result;
//    }
}