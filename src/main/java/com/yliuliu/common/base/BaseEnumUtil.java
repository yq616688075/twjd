package com.yliuliu.common.base;

/**
 * 创建时间 2017-12-08 0:30
 *
 * @author 陆国鸿
 */
public class BaseEnumUtil {

    private BaseEnumUtil() {
    }

    public static <T extends BaseEnum<?>> T parse(Class<T> enumType, Object code) {
        T[] enums = enumType.getEnumConstants();
        for (T t : enums) {
            if(t.code().equals(code)) {
                return t;
            }
        }
        throw new IllegalArgumentException("No enum constant " + enumType.getCanonicalName() + "." + code);
    }

    public static <T extends BaseEnum<?>> String getMsg(Class<T> enumType, Object code) {
        if (null == code) {
            return "";
        }
        T[] enums = enumType.getEnumConstants();
        for (T anEnum : enums) {
            if (code.equals(anEnum.code())) {
                return anEnum.msg();
            }
        }
        throw new IllegalArgumentException("No enum constant " + enumType.getCanonicalName() + "." + code);
    }

    public static <T extends BaseEnum<?>> Boolean containsCode(Class<T> enumType, Object code) {
        if (null == code) {
            return false;
        }
        T[] enums = enumType.getEnumConstants();
        for (T anEnum : enums) {
            if (code.equals(anEnum.code())) {
                return true;
            }
        }
        return false;
    }

}
