package com.yliuliu.common.base;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * JSON 工具类
 */
public final class JsonUtil {

    private static JsonFactory jsonFactory = new JsonFactory();

    private static ObjectMapper mapper = null;

    static {
        jsonFactory.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        jsonFactory.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper = new ObjectMapper(jsonFactory);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 获取jackson json lib的ObjectMapper对象
     *
     * @return -- ObjectMapper对象
     */
    public static ObjectMapper getMapper() {
        return mapper;
    }

    /**
     * 获取jackson json lib的JsonFactory对象
     *
     * @return --  JsonFactory对象
     */
    public static JsonFactory getJsonFactory() {
        return jsonFactory;
    }

    /**
     * 将json转成java bean
     *
     * @param <T>   -- 多态类型
     * @param json  -- json字符串
     * @param clazz -- java bean类型(Class)
     * @return -- java bean对象
     */
    public static <T> T toBean(String json, Class<T> clazz) {

        T rtv = null;
        try {
            rtv = mapper.readValue(json, clazz);
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java bean异常", ex);
        }
        return rtv;
    }

    public static Map<String, String> toBeanMap(String json) {

        try {
            Map<String, String> maps = mapper.readValue(json, Map.class);
            return maps;
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java map异常", ex);
        }
    }

    public static Map<String, Object> toBeanMapObject(String json) {

        try {
            Map<String, Object> maps = mapper.readValue(json, Map.class);
            return maps;
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java map object异常", ex);
        }
    }

    /**
     * 将java bean转成json
     *
     * @param bean -- java bean
     * @return -- json 字符串
     */
    public static String toJson(Object bean) {

        String rtv = null;
        try {
            rtv = mapper.writeValueAsString(bean);
        } catch (Exception ex) {
            throw new IllegalArgumentException("java bean转成json字符串异常", ex);
        }
        return rtv;
    }

    /**
     * 将java类型的对象转换为JSON格式的字符串
     *
     * @param object java类型的对象
     * @return JSON格式的字符串
     */
    public static <T> String serialize(T object) {
        return com.alibaba.fastjson.JSON.toJSONString(object);
    }

    /**
     * 将JSON格式的字符串转换为java类型的对象或者java数组类型的对象，不包括java集合类型
     *
     * @param json JSON格式的字符串
     * @param clz  java类型或者java数组类型，不包括java集合类型
     * @return java类型的对象或者java数组类型的对象，不包括java集合类型的对象
     */
    public static <T> T deserialize(String json, Class<T> clz) {
        return com.alibaba.fastjson.JSON.parseObject(json, clz);
    }

    /**
     * 将JSON格式的字符串转换为List<T>类型的对象
     *
     * @param json JSON格式的字符串
     * @param clz  指定泛型集合里面的T类型
     * @return List<T>类型的对象
     */
    public static <T> List<T> deserializeList(String json, Class<T> clz) {
        return com.alibaba.fastjson.JSON.parseArray(json, clz);
    }

    /**
     * 将JSON格式的字符串转换成任意Java类型的对象
     *
     * @param json JSON格式的字符串
     * @param type 任意Java类型
     * @return 任意Java类型的对象
     */
    public static <T> T deserializeAny(String json, TypeReference<T> type) {
        return com.alibaba.fastjson.JSON.parseObject(json, type);
    }


    public static String getJsonValue(String json, String key) {
        HashMap map = deserialize(json, HashMap.class);
        if (map.get(key) == null){
            return null;
        }
        return map.get(key).toString();
    }

    public static Integer getJsonInterValue(String json, String key) {
        HashMap map = deserialize(json, HashMap.class);
        if (map.get(key) == null){
            return null;
        }
        return (Integer) map.get(key);
    }

    public static void arrayCopy(JSONArray srcArray,JSONArray targetArray,String... ignoreProperties) {
        List<String> ignoreList = ignoreProperties != null? Arrays.asList(ignoreProperties):null;
        for (int i=0; i<srcArray.size(); i++) {
            JSONObject jsonObject = srcArray.getJSONObject(i);
            if (ignoreList != null) {
                for (String ignore : ignoreList) {
                    if (jsonObject.containsKey(ignore)) {
                        jsonObject.remove(ignore);
                    }
                }
            }
            targetArray.add(jsonObject);
        }
    }
}
