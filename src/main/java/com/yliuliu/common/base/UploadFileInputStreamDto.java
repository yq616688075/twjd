package com.yliuliu.common.base;

import lombok.Data;

import java.io.InputStream;
import java.io.Serializable;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-16
 */
@Data
public class UploadFileInputStreamDto implements Serializable {
    private InputStream inputStream;
    private String fileName;

    @Override
    public String toString() {
        return "UploadFileInputStreamDto(super=" + super.toString() + ", inputStream=no output, fileName=" + this.getFileName() + ")";
    }
}
