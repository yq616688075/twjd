package com.yliuliu.common.base;

/**
 * Created by HUANGZC8 on 2017/5/17.
 */
public class ParamUtil {

    public static String getParam(String url, String param) {
        String pattern = "(.*)([&|\\?]+)" + param + "=([^&]*)(.)*";
        if (url.matches(pattern)) {
            return url.replaceAll(pattern, "$3");
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getParam("http://aaa.bb.com?SOAPAction=fdfdfd&c=d", "SOAPAction"));
    }

}
