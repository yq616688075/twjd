package com.yliuliu.common.base;

import java.io.UnsupportedEncodingException;

/**
 * Created by pc on 2016/7/30.
 */
public class ChangeCharset {

    private final static char[] HEX = "0123456789abcdef".toCharArray();


    public static String bytes2HexString(byte[] bys) {
        char[] chs = new char[bys.length * 2 + bys.length - 1];
        for (int i = 0, offset = 0; i < bys.length; i++) {
            if (i > 0) {
                chs[offset++] = ' ';
            }
            chs[offset++] = HEX[bys[i] >> 4 & 0xf];
            chs[offset++] = HEX[bys[i] & 0xf];
        }
        return new String(chs);
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String str = "766679中国";
        System.out.print(bytes2HexString(str.getBytes("UTF-16BE")).replaceAll(" ", ""));

    }


    public static String toUTF_16BE(String str) throws UnsupportedEncodingException {

        return  bytes2HexString(str.getBytes("UTF-16BE")).replaceAll(" ", "");
    }




}


