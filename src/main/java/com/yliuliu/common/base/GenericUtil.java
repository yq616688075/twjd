package com.yliuliu.common.base;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by HUANGZC8 on 2016/9/21.
 */
public class GenericUtil {

    public static <T> Class<T> getGenericType(Class<?> clazz, int index) {
        Type genType = clazz.getGenericSuperclass();
        if (!(genType instanceof ParameterizedType)) {
            genType = clazz.getSuperclass().getGenericSuperclass();
        }
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        return (Class) params[index];
    }

    public static <T> Class<T> getGenericType(Class<?> clazz) {
        return getGenericType(clazz, 0);
    }

    public static <T> T newGenericInstance(Class<?> clazz) {
        return newGenericInstance(clazz, 0);
    }

    public static <T> T newGenericInstance(Class<?> clazz, int index) {
        try {
            return (T) getGenericType(clazz, index).newInstance();
        } catch (Exception e) {
            return null;
        }
    }

}
