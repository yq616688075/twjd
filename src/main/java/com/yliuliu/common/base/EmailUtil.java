package com.yliuliu.common.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class EmailUtil {

	private EmailUtil() {}

	public static boolean check(String email) {
		if (email != null) {
			Pattern p = Pattern.compile("^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)$");
			Matcher m = p.matcher(email);
			return m.matches();
		}
		return false;
	}
}
