package com.yliuliu.common.base;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.*;
import java.security.MessageDigest;
import java.util.*;

public final class FileUtil {

    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VOICE = "voice";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_DOC = "doc";

    private static final HashMap<String, Set<String>> typeNameMap = new HashMap<String, Set<String>>() {
        private static final long serialVersionUID = -6081111304347917746L;

        {
            // 文件后缀名必须是小写
            put(TYPE_IMAGE, new HashSet<>(Arrays.asList("bmp", "jpg","jpeg","png", "gif", "tiff", "pcx", "tga", "exif", "fpx", "svg", "psd", "cdr", "pcd", "dxf", "ufo", "eps", "ai", "raw")));
            put(TYPE_VOICE, new HashSet<>(Arrays.asList("mp3", "wma", "midi", "rmi", "amr")));
            put(TYPE_VIDEO, new HashSet<>(Arrays.asList("avi", "wmv", "rmvb", "rm", "swf", "flv", "mp4", "mid", "3gp", "mkv", "mov", "mpg", "mpeg")));
            put(TYPE_DOC, new HashSet<>(Arrays.asList("doc", "ppt", "xls", "xml", "docx", "xlsx", "pptx", "html", "htm", "pdf")));
        }
    };

    private FileUtil() {
    }

    private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

    public static String getFileExtraName(File file) {
        String fileName = file.getName();
        return getFileExtraName(fileName);
    }

    public static String getFileExtraName(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index >= 0 && index < fileName.length() - 1) {
            return fileName.substring(index + 1).toLowerCase();
        }
        return null;
    }


    public static String getFileName(String filePath) {
        int index = filePath.lastIndexOf('/');
        if (index >= 0 && index < filePath.length() - 1) {
            return filePath.substring(index + 1).toLowerCase();
        }
        return null;
    }

    public static String getBase64(File file) {
        try {
            return getBase64(new FileInputStream(file));
        }
        catch (Exception exp) {
            logger.error("make base64 failed by file", exp);
        }
        return null;
    }

    public static String getBase64(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }

        try {
            byte[] buffer = new byte[inputStream.available()];
            int read = inputStream.read(buffer);
            logger.debug("read {} bytes from input stream.", read);
            return Base64Utils.encode(buffer);
        }
        catch (Exception exp) {
            logger.error("make base64 failed by input stream", exp);
            return null;
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException e) {
                logger.error("close input stream failed.", e);
            }
        }
    }

    public static FileStruct fromBase64(String base64, String path) {

        FileStruct fileStruct = new FileStruct();
        fileStruct.setPath(path);
        FileOutputStream out = null;

        try {

            byte[] buffer = Base64Utils.decode(base64);
            File file = new File(path);
            out = new FileOutputStream(file);
            out.write(buffer);

            fileStruct.setSize(buffer.length);

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(buffer);

            byte[] byteArray = messageDigest.digest();
            StringBuffer md5StrBuff = new StringBuffer();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(0xFF & byteArray[i]).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
                }
                else {
                    md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
                }
            }
            fileStruct.setMd5(md5StrBuff.toString());

            return fileStruct;
        }
        catch (Exception exp) {
            logger.error("", exp);
            return null;
        }
        finally {
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException e) {
                    logger.error("", e);
                }
            }
        }
    }

    public static boolean isImage(String fileName) {
        return isFileType(TYPE_IMAGE, fileName);
    }

    public static boolean isDoc(String fileName) {
        return isFileType(TYPE_DOC, fileName);
    }

    public static boolean isVideo(String fileName) {
        return isFileType(TYPE_VIDEO, fileName);
    }

    public static boolean isVoice(String fileName) {
        return isFileType(TYPE_VOICE, fileName);
    }

    private static boolean isFileType(String type, String fileName) {
        Set<String> fileExtraNames = typeNameMap.get(type);
        if (fileExtraNames == null) {
            return false;
        }

        String extraName = getFileExtraName(fileName);
        return fileExtraNames.contains(extraName);
    }

    public static FileStruct getFileStruct(String filePath) {
        FileStruct fileStruct = new FileStruct();
        fileStruct.setPath(filePath);
        File file = new File(filePath);

        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            fileStruct.setSize(stream.available());
            byte[] buffer = new byte[stream.available()];
            int read = stream.read(buffer);
            logger.debug("read {} bytes from file: {}", read, filePath);

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(buffer);

            byte[] byteArray = messageDigest.digest();
            StringBuffer md5StrBuff = new StringBuffer();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(0xFF & byteArray[i]).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
                }
                else {
                    md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
                }
            }
            fileStruct.setMd5(md5StrBuff.toString());
        }
        catch (Exception exp) {
            logger.error("", exp);
            return null;
        }
        finally {
            if (stream != null) {
                try {
                    stream.close();
                }
                catch (IOException exp) {
                    logger.error("", exp);
                }
            }
        }
        return fileStruct;
    }

    public static String makeFileNameWithPath(String rootPath, String extraName) {
        if (StringUtils.isEmpty(extraName)) {
            return null;
        }

        if (StringUtils.isEmpty(rootPath)) {
            return null;
        }

        StringBuffer buffer = new StringBuffer(StringUtils.cleanPath(rootPath));
        buffer.append(File.separator);

        String dateTimeStr = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS");
        buffer.append(dateTimeStr.substring(0, 6)); // e.g.  201501, 2015年1月
        buffer.append(File.separator);
        buffer.append(dateTimeStr.substring(6, 8)); // e.g.  21, 21日
        buffer.append(File.separator);
        buffer.append(dateTimeStr.substring(8, 10)); // e.g. 22, 22点
        buffer.append(File.separator);
        String sPath = buffer.toString();
        if (!fileExists(sPath)) {
            makeDirs(sPath);
        }


        String sRandom = RandomStringUtils.randomNumeric(4);

        if (extraName.charAt(0) != '.') {
            extraName = "." + extraName;
        }

        // 构造文件名称
        // 在检查文件名重复时，最多允许构造两次。
        buffer.append(dateTimeStr.substring(10)); // e.g. 1704231, 17分04秒221
        buffer.append(sRandom);
        buffer.append(extraName);
        String sFileName = buffer.toString();
        for (int i = 0; i < 2; i++) {
            if (i > 0) { // 扩展随机数以解决重复问题，每次向后扩展两位
                int offset = buffer.length() - extraName.length();
                buffer.insert(offset, RandomStringUtils.randomNumeric(2));
                sFileName = buffer.toString();
            }

            // 检查文件名是否重复
            if (!fileExists(sFileName)) {
                return sFileName; // 找到不重复的文件名
            }
        }// end for
        return null; // 没有找到不重复的文件名
    }

    public static boolean fileExists(String _sPathFileName) {
        File file = new File(_sPathFileName);
        return file.exists();
    }

    public static boolean makeDirs(String _sDir) {
        boolean zResult = false;
        File file = new File(_sDir);
        zResult = file.mkdirs(); // 如果父目录不存在，则创建所有必需的父目录
        if (!zResult) {
            zResult = file.exists();
        }
        return zResult;
    }

    /**
     * 根据byte数组，生成文件
     */
    public static void getFile(byte[] bfile, String filePath,String fileName) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            File dir = new File(filePath);
            if(!dir.exists()&&dir.isDirectory()){//判断文件目录是否存在
                dir.mkdirs();
            }
            file = new File(filePath+"/"+fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * 获得指定文件的byte数组
     */
    public static byte[] getBytes(String filePath){
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }


    public static class FileStruct {
        private String path;
        private int size;
        private String md5;

        public String getMd5() {
            return md5;
        }

        public void setMd5(String md5) {
            this.md5 = md5;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }
}
