package com.yliuliu.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 创建时间 2017-08-30 15:03
 *
 * @author 陆国鸿
 */
public class BaseProperty {

    private static final Logger logger = LoggerFactory.getLogger(BaseProperty.class);

    private BaseProperty() {}

    synchronized static public Properties loadProps(String loadFileName){
        logger.info("开始加载{}文件内容.......", loadFileName);
        Properties props = new Properties();
        InputStream in = null;
        try {
            // 第一种，通过类加载器进行获取properties文件流
            in = BaseProperty.class.getClassLoader()
                    .getResourceAsStream(loadFileName);
            props.load(in);
        } catch (FileNotFoundException e) {
            logger.error("{}文件未找到", loadFileName);
        } catch (IOException e) {
            logger.error("出现IOException");
        } finally {
            try {
                if(null != in) {
                    in.close();
                }
            } catch (IOException e) {
                logger.error("{}文件流关闭出现异常", loadFileName);
            }
        }
        logger.info("加载{}文件内容完成...........", loadFileName);
        return props;
    }
}
