package com.yliuliu.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;

public final class ConfigUtil {

    private static Logger logger = LoggerFactory.getLogger(ConfigUtil.class);

    private ConfigUtil() {
    }

    public static String getHostName() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            return address.getHostName();

        } catch (UnknownHostException exp) {
            logger.error("get host name failed.", exp);
        }
        return "unknown";
    }

    public static String getHostIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }

                }
            }
        } catch (SocketException ex) {
            logger.error("get host ip failed.", ex);
        }
        return "";
    }

    /**
     * 仅限http服务容器中调用
     * @return
     */
    public static String getHostPort() {
        return getPort("HTTP/1.1", "http");
    }


    private static String getPort(String protocol, String scheme) {
        MBeanServer mBeanServer = null;
        ArrayList<MBeanServer> beanServers = MBeanServerFactory.findMBeanServer(null);
        if (!CollectionUtils.isEmpty(beanServers)) {
            mBeanServer = beanServers.get(0);
        }

        if (mBeanServer == null) {
            return "";
        }

        Set names;
        try {
            names = mBeanServer.queryNames(new ObjectName("Catalina:type=Connector,*"), null);
        }
        catch (MalformedObjectNameException e) {
            return "";
        }

        if (names == null) {
            return "";
        }

        Iterator it = names.iterator();
        ObjectName oname;
        try {

            while (it.hasNext()) {
                oname = (ObjectName) it.next();
                String pvalue = (String) mBeanServer.getAttribute(oname, "protocol");
                String svalue = (String) mBeanServer.getAttribute(oname, "scheme");
                if (protocol.equals(pvalue) && scheme.equals(svalue)) {
                    return ((Integer) mBeanServer.getAttribute(oname, "port")).toString();
                }
            }
        } catch (Exception exp) {
            logger.error("get service port failed.", exp);
        }
        return "";
    }
}
