package com.yliuliu.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * JSON 工具类
 */
public final class JsonUtil {


    /**
     * 将json转成java bean
     *
     * @param <T>   -- 多态类型
     * @param json  -- json字符串
     * @param clazz -- java bean类型(Class)
     * @return -- java bean对象
     */
    public static <T> T toBean(String json, Class<T> clazz) {

        T rtv = null;
        try {
            rtv = JSON.parseObject(json, clazz);
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java bean异常", ex);
        }
        return rtv;
    }

    public static Map<String, String> toBeanMap(String json) {

        try {
            Map<String, String> maps = JSON.parseObject(json, HashMap.class);
            return maps;
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java map异常", ex);
        }
    }

    public static Map<String, Object> toBeanMapObject(String json) {

        try {
            Map<String, Object> maps = JSON.parseObject(json, HashMap.class);
            return maps;
        } catch (Exception ex) {
            throw new IllegalArgumentException("json字符串转成java map object异常", ex);
        }
    }

    /**
     * 将java bean转成json
     *
     * @param bean -- java bean
     * @return -- json 字符串
     */
    public static String toJson(Object bean) {

        String rtv = null;
        try {
            rtv = JSON.toJSONString(bean);
        } catch (Exception ex) {
            throw new IllegalArgumentException("java bean转成json字符串异常", ex);
        }
        return rtv;
    }

    /**
     * 将java类型的对象转换为JSON格式的字符串
     *
     * @param object java类型的对象
     * @return JSON格式的字符串
     */
    public static <T> String serialize(T object) {
        return JSON.toJSONString(object);
    }

    /**
     * 将JSON格式的字符串转换为java类型的对象或者java数组类型的对象，不包括java集合类型
     *
     * @param json JSON格式的字符串
     * @param clz  java类型或者java数组类型，不包括java集合类型
     * @return java类型的对象或者java数组类型的对象，不包括java集合类型的对象
     */
    public static <T> T deserialize(String json, Class<T> clz) {
        return JSON.parseObject(json, clz);
    }

    /**
     * 将JSON格式的字符串转换为List<T>类型的对象
     *
     * @param json JSON格式的字符串
     * @param clz  指定泛型集合里面的T类型
     * @return List<T>类型的对象
     */
    public static <T> List<T> deserializeList(String json, Class<T> clz) {
        return JSON.parseArray(json, clz);
    }

    /**
     * 将JSON格式的字符串转换成任意Java类型的对象
     *
     * @param json JSON格式的字符串
     * @param type 任意Java类型
     * @return 任意Java类型的对象
     */
    public static <T> T deserializeAny(String json, TypeReference<T> type) {
        return JSON.parseObject(json, type);
    }


    public static String getJsonValue(String json, String key) {
        HashMap map = deserialize(json, HashMap.class);
        if (map.get(key) == null){
            return null;
        }
        return map.get(key).toString();
    }

    public static Integer getJsonInterValue(String json, String key) {
        HashMap map = deserialize(json, HashMap.class);
        if (map.get(key) == null){
            return null;
        }
        return (Integer) map.get(key);
    }
}
