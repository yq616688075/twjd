package com.yliuliu.common.util.excel.supplier;

/**
 *
 * 意为简单实现，未经任何封装的类型。
 * 适用于数据量少的场景
 * @param <T> 数据
 */
public interface SimpleSupplier<T> extends DataSupplier<T>{
}
