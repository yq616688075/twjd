package com.yliuliu.common.util.excel;

import com.yliuliu.common.util.excel.supplier.DataSupplier;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * sheet实体
 * @param <T>
 */

@Accessors(chain = true)
@Getter
@Setter
public  class SheetInfo<T> {

    /**
     * sheet名字
     */
    private String sheetName;

    /**
     * 实体类
     */
    private Class<T> entity;

    /**
     * 数据列
     */
    private List<ColumnInfo<T>> columnList = new ArrayList<>();

    /**
     * 数据列表
     */
    private DataSupplier<List<T>> dataSupplier;


    /**
     * 增加一列
     * @param header 表头
     * @param fieldConversion 转换器
     * @return 当前sheet
     */
    public SheetInfo<T> addColumn(String header,FieldConversion<T> fieldConversion){
        columnList.add(new ColumnInfo<T>(header,fieldConversion));
        return this;
    }

    /**
     *
     * @param header 表头
     * @param fieldName 属性名
     * @return 当前sheet
     */
    public SheetInfo<T> addColumn(String header, String fieldName){
        columnList.add(new ColumnInfo<T>(header,fieldName));
        return this;
    }

    public SheetInfo<T> addColumn(ColumnInfo<T> columnInfo){
        columnList.add(columnInfo);
        return this;
    }

}
