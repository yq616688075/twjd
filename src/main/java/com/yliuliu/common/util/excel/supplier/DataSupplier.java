package com.yliuliu.common.util.excel.supplier;

import java.util.function.Supplier;

public interface DataSupplier<T> extends Supplier<T> {
}
