package com.yliuliu.common.util.excel;

import com.yliuliu.common.base.DateUtil;
import com.yliuliu.common.util.excel.supplier.DataSupplier;
import com.yliuliu.common.util.excel.supplier.DataflowSupplier;
import com.yliuliu.common.util.excel.supplier.SimpleSupplier;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.util.CollectionUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * 生成excel文件。
 * 1.可以生成多个sheet
 * 2.可以生成根据列转换器转换数据
 * 3.可以根据属性类型写数据
 */
@Slf4j
public class ExcelUtil {


    private static Map<Class<?>, Map<String, PropertyDescriptor>> registerEntityMap = new ConcurrentHashMap<>();


    private ExcelUtil() {
    }

    /**
     * 单例
     */
    private volatile static ExcelUtil singleton;

    private static ExcelUtil getInstance() {
        if (singleton == null) {
            synchronized (ExcelUtil.class) {
                if (singleton == null) {
                    singleton = new ExcelUtil();
                }
            }
        }
        return singleton;
    }


    /**
     * 反射实体列字段
     *
     * @param clazz 实体类
     * @return readMethodMap
     */
    private static Map<String, PropertyDescriptor> parseClass(Class<?> clazz) {
        Map<String, PropertyDescriptor> objPropertyMap = new HashMap<>();
        BeanInfo beanInfo;
        PropertyDescriptor[] propertyDescriptors;
        try {
            beanInfo = Introspector.getBeanInfo(clazz);
            propertyDescriptors = beanInfo.getPropertyDescriptors();
        } catch (Exception e) {
            log.error("反射异常");
            return null;
        }
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            objPropertyMap.put(propertyDescriptor.getName(), propertyDescriptor);
        }
        return objPropertyMap;
    }


    public static class Builder {

        /**
         * 初始化ExcelUtil
         */
        static {
            ExcelUtil.getInstance();
        }

        /**
         * 工作簿
         */
        private SXSSFWorkbook wb;

        /**
         * sheet信息列表
         */
        private List<SheetInfo<?>> sheetInfoList = new ArrayList<>();

        private Builder(SXSSFWorkbook wb) {
            this.wb = wb;
        }


        public static Builder newInstance() {
            return new Builder(new SXSSFWorkbook());
        }

        public static Builder newInstance(int rowAccessWindowSize) {
            return new Builder(new SXSSFWorkbook(rowAccessWindowSize));
        }

        /**
         * @param sheetInfo 一页sheet
         * @param <T>       实体类
         * @return 一页sheet
         */
        public <T> Builder createSheet(SheetInfo<T> sheetInfo) {
            //解析实体类，并缓存
            Class<T> entity = sheetInfo.getEntity();
            registerEntityMap.putIfAbsent(entity, parseClass(entity));
            //保存sheet
            sheetInfoList.add(sheetInfo);
            return this;
        }

        /**
         * 把所有sheet写到硬盘，生成Excel文件
         */
        public void flush(OutputStream outputStream) {
            try {
                sheetInfoList.forEach(this::writeSheet);
                wb.write(outputStream);
            } catch (IOException e) {
                // e.printStackTrace();
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    //将实例销毁
                } catch (IOException e) {
                    log.error("关闭流失败", e);
                }
            }
        }

        private <T> void writeSheet(SheetInfo<T> sheetInfo) {
            //创建工作簿
            Sheet sheet = wb.createSheet(sheetInfo.getSheetName());
            //表头
            List<String> headList = sheetInfo.getColumnList().stream().map(ColumnInfo::getHeader).collect(Collectors.toList());
            //写表头
            Row row = sheet.createRow(0);
            //冻结表头
            sheet.createFreezePane(0, 1, 0, 1);
            for (int i = 0; i < headList.size(); i++) {
                Cell cell = row.createCell(i);
                cell.setCellValue(headList.get(i));//写入内容
            }
            //获取反射方法
            Map<String, PropertyDescriptor> objPropertyMap = registerEntityMap.get(sheetInfo.getEntity());
            //数据
            DataSupplier<List<T>> dataSupplier = sheetInfo.getDataSupplier();
            if (dataSupplier instanceof SimpleSupplier) {
                List<T> dataList = dataSupplier.get();
                writerRow(sheet, sheetInfo.getColumnList(), dataList, objPropertyMap);
            } else if (dataSupplier instanceof DataflowSupplier) {
                List<T> dataList = dataSupplier.get();
                while (!CollectionUtils.isEmpty(dataList)) {
                    writerRow(sheet, sheetInfo.getColumnList(), dataList, objPropertyMap);
                    dataList = dataSupplier.get();
                }
            }
        }

        private <T> FieldConversion<T> getDefaultFieldConversion(PropertyDescriptor propertyDescriptor){
            return dataList -> {
                Method readMethod = propertyDescriptor.getReadMethod();
                return dataList.stream().map(data->{
                    Object value = null;
                    try {
                        value = readMethod.invoke(data);
                    } catch (Exception e) {
                        log.error("");
                    }
                    //null值处理
                    if (value == null) {
                        value = "";
                    }
                    //数据类型处理
                    if (value instanceof Date) {
                        //时间类型
                        value = DateUtil.format((Date) value);
                    } else if (value instanceof Number) {
                        if(value instanceof  Long){
                            value = Long.parseLong(value.toString());
                        }else{
                            //数字类型
                            value = Double.parseDouble(value.toString());
                        }
                    }
                    return value.toString();
                }).collect(Collectors.toList());
            };
        }

        //续写数据
        private <T> void writerRow(Sheet sheet, List<ColumnInfo<T>> exportColumns, List<T> dataList, Map<String, PropertyDescriptor> objPropertyMap) {
            // 数据转换
            List<List<String>> outputDataList = new ArrayList<>(exportColumns.size());
            for (ColumnInfo<T> exportColumn : exportColumns) {
                List<String> afterConvert;
                FieldConversion<T> fieldConversion = exportColumn.getFieldConversion();
                if(fieldConversion == null){
                    FieldConversion<T> defaultFieldConversion = getDefaultFieldConversion(objPropertyMap.get(exportColumn.getFiledName()));
                    afterConvert = defaultFieldConversion.convert(dataList);
                }else {
                    afterConvert = fieldConversion.convert(dataList);
                }
                outputDataList.add(afterConvert);
            }
            int lastRowNum = sheet.getLastRowNum();
            lastRowNum++;
            for (int i = 0; i < dataList.size(); i++) {
                Row row = sheet.createRow(lastRowNum + i);
                for (int j = 0; j < outputDataList.size(); j++) {
                    Cell cell = row.createCell(j);
                    String value = "";
                    if(!CollectionUtils.isEmpty(outputDataList.get(j)) && outputDataList.get(j).size() > i){
                        value = outputDataList.get(j).get(i);
                    }
                    cell.setCellValue(value);
                }
            }
        }
    }

}

