package com.yliuliu.common.util.excel.supplier;

/**
 * DataflowSupplier适用于大量数据的情况
 * 流式处理数据只有get方法的返回值为null或集合长度为空时，才停止抓取，否则将一直运行下去
 *
 * 结合mybatis-plus的动态sql可以很好的抓取数据
 */
public interface DataflowSupplier<T> extends DataSupplier<T>{
}
