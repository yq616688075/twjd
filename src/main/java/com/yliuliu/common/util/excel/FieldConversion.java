package com.yliuliu.common.util.excel;


import java.util.List;

@FunctionalInterface
public interface FieldConversion<T> {



    /**
     *
     * @param dataList 转换方法
     * @return 转换后的数据
     */
    List<String> convert(List<T> dataList);


}