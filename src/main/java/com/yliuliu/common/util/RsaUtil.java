package com.yliuliu.common.util;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.util.IOUtils;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class RsaUtil {

    public static final String CHARSET = "UTF-8";
    public static final String RSA_ALGORITHM = "RSA";

    public static Map<String, String> createKeys(int keySize){
        //为RSA算法创建一个KeyPairGenerator对象
        KeyPairGenerator kpg;
        try{
            kpg = KeyPairGenerator.getInstance(RSA_ALGORITHM);
        }catch(NoSuchAlgorithmException e){
            throw new IllegalArgumentException("No such algorithm-->[" + RSA_ALGORITHM + "]");
        }

        //初始化KeyPairGenerator对象,密钥长度
        kpg.initialize(keySize);
        //生成密匙对
        KeyPair keyPair = kpg.generateKeyPair();
        //得到公钥
        Key publicKey = keyPair.getPublic();
        String publicKeyStr = Base64.encodeBase64URLSafeString(publicKey.getEncoded());
        //得到私钥
        Key privateKey = keyPair.getPrivate();
        String privateKeyStr = Base64.encodeBase64URLSafeString(privateKey.getEncoded());
        Map<String, String> keyPairMap = new HashMap<String, String>();
        keyPairMap.put("publicKey", publicKeyStr);
        keyPairMap.put("privateKey", privateKeyStr);

        return keyPairMap;
    }

    /**
     * 得到公钥
     * @param publicKey 密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //通过X509编码的Key指令获得公钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(x509KeySpec);
        return key;
    }

    /**
     * 得到私钥
     * @param privateKey 密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //通过PKCS#8编码的Key指令获得私钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
        RSAPrivateKey key = (RSAPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
        return key;
    }

    /**
     * 公钥加密
     * @param data
     * @param publicKey
     * @return
     */
    public static String publicEncrypt(String data, RSAPublicKey publicKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return Base64.encodeBase64URLSafeString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), publicKey.getModulus().bitLength()));
        }catch(Exception e){
            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
        }
    }



    /**
     * 私钥解密
     * @param data
     * @param privateKey
     * @return
     */

    public static String privateDecrypt(String data, RSAPrivateKey privateKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data), privateKey.getModulus().bitLength()), CHARSET);
        }catch(Exception e){
            throw new RuntimeException("RSA解密异常", e);
        }
    }

    /**
     * 私钥加密
     * @param data
     * @param privateKey
     * @return
     */

    public static String privateEncrypt(String data, RSAPrivateKey privateKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            return Base64.encodeBase64URLSafeString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), privateKey.getModulus().bitLength()));
        }catch(Exception e){
            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 公钥解密
     * @param data
     * @param publicKey
     * @return
     */

    public static String publicDecrypt(String data, RSAPublicKey publicKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data), publicKey.getModulus().bitLength()), CHARSET);
        }catch(Exception e){
            throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
        }
    }

    private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas, int keySize){
        int maxBlock = 0;
        if(opmode == Cipher.DECRYPT_MODE){
            maxBlock = keySize / 8;
        }else{
            maxBlock = keySize / 8 - 11;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] buff;
        int i = 0;
        try{
            while(datas.length > offSet){
                if(datas.length-offSet > maxBlock){
                    buff = cipher.doFinal(datas, offSet, maxBlock);
                }else{
                    buff = cipher.doFinal(datas, offSet, datas.length-offSet);
                }
                out.write(buff, 0, buff.length);
                i++;
                offSet = i * maxBlock;
            }
        }catch(Exception e){
            throw new RuntimeException("加解密阀值为["+maxBlock+"]的数据时发生异常", e);
        }
        byte[] resultDatas = out.toByteArray();
        IOUtils.closeQuietly(out);
        return resultDatas;
    }

    public static void main (String[] args) throws Exception {
        Map<String, String> keyMap = RsaUtil.createKeys(2048);
       /* String  publicKey = keyMap.get("publicKey");
        String  privateKey = keyMap.get("privateKey");*/

        String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg238E+OTRfKpgqjYHFq24hX2Uct28uFAPEJTLjo2qzrCOopaQaSlus+Yx4mt1M94kr2vpzaRrDpBNGe1HNf1oJUHLvdBUamAKCVr/vlmljBIrMoSJ2DfZfawlezAmkjQknTAISOMQqKu2fHJomn5LThlt/elrAjPAvfzsqIhjpx5I5a+iMLVilONWme9lQccPWKeA2YpvT4MXxlhAzfUPyGnMaLsDVHU+6mDWisQX2txD/lV0onDOVD4+ho8kfrOxWAiJrTUpPZwpp7n8Rtpe6qLeG8j1WRQVR2WWHckDVEqhNwbVBkIdPTmoF9oi/snXANfB52sZJ59RpBpZurTowIDAQAB";
        String  privateKey ="MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCDbfwT45NF8qmCqNgcWrbiFfZRy3by4UA8QlMuOjarOsI6ilpBpKW6z5jHia3Uz3iSva+nNpGsOkE0Z7Uc1/WglQcu90FRqYAoJWv++WaWMEisyhInYN9l9rCV7MCaSNCSdMAhI4xCoq7Z8cmiafktOGW396WsCM8C9/OyoiGOnHkjlr6IwtWKU41aZ72VBxw9Yp4DZim9PgxfGWEDN9Q/IacxouwNUdT7qYNaKxBfa3EP+VXSicM5UPj6GjyR+s7FYCImtNSk9nCmnufxG2l7qot4byPVZFBVHZZYdyQNUSqE3BtUGQh09OagX2iL+ydcA18Hnaxknn1GkGlm6tOjAgMBAAECggEAKL6HAC9z76Jb1Iq9fITO32YJl6/o70DPZh71pIYdfpXXsshDVMhTLIfBCFLxVeD/sxYlB0SOgoMIxdNEvW1h02d/85ppYISYm1mGkmQE3piXXyHB4P1SJE0Gz2O3Dd1284BIM73OdLX0WNJxPO13dwJd3ltiu+N7MaNmpEFI/+mmie/xCLl1KHfP70azDiZHm8qDd1A0EwSU60qwESGLa5lqx4imoPwHGdSnEAIC7NJMkGhpp1k9nbAEy2iFbzOgtQpEYoYuuEjdXKR/R+naMZusFit52ctbJ4w9Vkz/Qlg0emXy1ts3UMEpWaq3eKlF/5HEdWovphSIJLO8UW7CUQKBgQC8pJ1Zm6+vj/Wmnnvg9Oj1J43WmjqZy1XcSOahg6lWV3Jjhf0kW+xRI/7LT9vITvazIGxShSrhIphQEMkSlgSxgaExn3ww7lfEFTkEwyPcx7GeDJVPF56+cN/zCaE0NggwdByTx52ZgquvdBqWw0akVE6RpIV3ZztDLOKop1C5KQKBgQCyW6MOtN8PbKbaFQ90FTIGwIaUDeKgf7o/fGD/yYZyJarf6ib0xM4oyC8pbzyvL+dMgvywPoDt+MMwBNUD71KQUnAoP9IlTSkhZ22f/Vp5HcqP7d28oi/ngv/Y+nXAeOC8pN8qcpzyrsr40PdgWfoevxTHP5qhuhZTGKjdH0Zj6wKBgCViijyD+iFU6l4G8o32e2R6XkW1cv+fqHhriilgKmMOGYFXwH9AKGk8NPad8PSVkZaWSmFgOss7S+wyJ2cji/KRuhxXewlHcuVNP2uOMMFEYhLsc3qlN7SOMhO8SNJMEvtnR3KSEM2nc9blr49JS+qtOpTqgfjT4I7DinfaEMlhAoGAGif8Kqz1xWJOuc663HA7TOgxFEdRA+6XVrYb14crU1QY0guZuO2fujVZTT9F6/8fcYYIHqSb7co8fa9HufL/7bBZSF3LqLK/JGnxlrPl77shuKfOi9pRIieA88YPPLG9a4zWOXhjceeWVpT2okevuZCWMH4wZCVRHtCP+xWaEg8CgYBOjslZV0FxHjouTouRyoFLqixNfd2Sw9H5elFHZUAzdFUMdpc02Hrj3bLOIXN3bBbUgkhHd2ggWrVLPV+30RIVJGL0e/8wilSOXPAeWY9WKC9pMvlKWa+KgNOax65BGUS70eaKnFpfj1m1XsoqQaOuBaFKK0VKOWgUIVqDcb246A==";

        System.out.println("公钥: \n\r" + publicKey);
        System.out.println("私钥： \n\r" + privateKey);

        System.out.println("公钥加密——私钥解密");
        String str = "Chm123456";
        System.out.println("\r明文：\r\n" + str);
        System.out.println("\r明文大小：\r\n" + str.getBytes().length);
        String encodedData = "caaR29jv7novB6s5WkXFpsJP2Xq6ygcVAcNBw1PNYNBv4X/+7b8FgsHx+tHjxeoVqgswTMiS+tELpSBIP00SbklWUV10fHqEutrIC/frFJmgCLhqI5HiQV163r5MHQ+UeLBntDqC7fNJaFNOtOr0CdTe/W9YetNatrugV8p+wHIiCB1JQjCHJXhqtriPJ4R7mBIJ1Jte2kBdlocRcHFmCo7/42HBqv+UtDvJRSLPAE7RDCwn2Gwv/msmJsmGY8nlqCBME8wJjJfhds8o1HclR90h3vNyX4GYVgfTm5CGdCdAVzDA1ob9A6OAHP1+4+G2AVJPmn2tr0v39PGSF23dnQ==";
        System.out.println("密文：\r\n" + encodedData);
        String decodedData = RsaUtil.privateDecrypt(encodedData, RsaUtil.getPrivateKey(privateKey));
        System.out.println("解密后文字: \r\n" + decodedData);


    }
}
