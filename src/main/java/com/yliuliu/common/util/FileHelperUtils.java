package com.yliuliu.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by WANGBING15 on 2016/6/14.
 */
public class FileHelperUtils {

    private static Logger logger = LoggerFactory.getLogger(FileHelperUtils.class);

    public static void fileDownload(HttpServletResponse response, String fileName, String filePath) {
        FileInputStream in = null;
        OutputStream out = null;
        try {
            //设置文件ContentType类型，这样设置，会自动判断下载文件类型
            response.setContentType("multipart/form-data");
            //设置响应头，控制浏览器下载该文件
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            //读取要下载的文件，保存到文件输入流
            in = new FileInputStream(filePath);
            //创建输出流
            out = response.getOutputStream();

            //创建缓冲区
            byte buffer[] = new byte[1024];
            int len = 0;
            //循环将输入流中的内容读取到缓冲区当中
            while ((len = in.read(buffer)) > 0) {
                //输出缓冲区的内容到浏览器，实现文件下载
                out.write(buffer, 0, len);
            }
            out.flush();
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭文件输入流
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //关闭输出流
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除单个文件
     * @param   sPath    被删除文件的文件名
     */
    public static void deleteFile(String sPath) {
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
        }
    }


//    public static String downloadFromUrl(String url,String dir) {
//
//        try {
//            URL httpurl = new URL(url);
//            String fileName = getFileNameFromUrl(url);
//            File f = new File(dir + fileName);
//            FileUtils.copyURLToFile(httpurl, f);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "Fault!";
//        }
//        return "Successful!";
//    }

    public static String getFileNameFromUrl(String url){
        String name = new Long(System.currentTimeMillis()).toString() + ".X";
        int index = url.lastIndexOf("/");
        if(index > 0){
            name = url.substring(index + 1);
            if(name.trim().length()>0){
                return name;
            }
        }
        return name;
    }


    public static void getURLResource(HttpServletRequest request, HttpServletResponse response, String urlStr, String attName) throws Exception {

        String method ="getURLResource()";
        InputStream in = null;
        OutputStream out = null;
        try {
            if(!StringUtils.hasText(attName)) {
                attName = getFileNameFromUrl(urlStr);
            }

            logger.info(" {},urlStr={} ,attName={}" , method,urlStr,attName);

            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            //设置超时间为3秒
//            conn.setConnectTimeout(3*1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            //得到输入流
            in = conn.getInputStream();

            //设置文件ContentType类型，这样设置，会自动判断下载文件类型
            response.setContentType("multipart/form-data");
            //设置响应头，控制浏览器下载该文件
            boolean isMSIE = HttpUtils.isMSBrowser(request);
            if (isMSIE) {
                attName = URLEncoder.encode(attName, "UTF-8");
            } else {
                attName = new String(attName.getBytes("UTF-8"), "ISO-8859-1");
            }
            response.setHeader("content-disposition", "attachment;filename=\"" + attName+ "\"");
            //创建输出流
            out = response.getOutputStream();

            //创建缓冲区
            byte buffer[] = new byte[1024];
            int len = 0;
            //循环将输入流中的内容读取到缓冲区当中
            while ((len = in.read(buffer)) > 0) {
                //输出缓冲区的内容到浏览器，实现文件下载
                out.write(buffer, 0, len);
            }
            out.flush();
            in.close();
            out.close();

        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        } finally {
            //关闭文件输入流
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //关闭输出流
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
