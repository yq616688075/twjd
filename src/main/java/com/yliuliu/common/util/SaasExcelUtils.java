package com.yliuliu.common.util;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 创建时间 2019-03-28 2:38 PM
 *
 * @author 陆国鸿
 */
@Slf4j
public class SaasExcelUtils {

    public static final int BIG_DATA_EXPORT_MIN = 3;

    public static final int BIG_DATA_EXPORT_MAX = 2000000;

    public static final String DEFAULT_SHEET_NAME = "Sheet1";

    public static final String BIG_FILE_EXT_FILE_NAME_XLSX = ".xlsx";

    public static final String BIG_FILE_EXT_FILE_NAME_XLS = ".xls";

    public static void exportExcelToResponse(String fileName, Class<?> pojoClass, Collection<?> dataSet, HttpServletResponse response) {
        exportExcelToResponse(fileName, null, pojoClass, dataSet, response, null);
    }

    public static void exportExcelToResponse(String fileName, Class<?> pojoClass, Collection<?> dataSet
            , HttpServletResponse response, HttpServletRequest request) {
        exportExcelToResponse(fileName, null, pojoClass, dataSet, response, request);
    }

    public static void exportExcelToResponse(String fileName, ExportParams exportParams, Class<?> pojoClass, Collection<?> dataSet, HttpServletResponse response) {
        if (null == exportParams) {
            exportParams = new ExportParams(null, DEFAULT_SHEET_NAME);
        }
        fileName = adapterFileName(fileName, exportParams);
        Workbook workbook = exportExcel(exportParams, pojoClass, dataSet);
        exportExcelToResponse(fileName, workbook, response, null);
    }

    public static void exportExcelToResponse(String fileName, ExportParams exportParams
            , Class<?> pojoClass, Collection<?> dataSet, HttpServletResponse response, HttpServletRequest request) {
        if (null == exportParams) {
            exportParams = new ExportParams(null, DEFAULT_SHEET_NAME);
        }
        fileName = adapterFileName(fileName, exportParams);
        Workbook workbook = exportExcel(exportParams, pojoClass, dataSet);
        exportExcelToResponse(fileName, workbook, response, request);
    }

    public static void exportExcelToResponse(String fileName, Workbook workbook, HttpServletResponse response, HttpServletRequest request) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            DownloadUtil.createDownloadFileContentDispositionContent(request, response, fileName);
            workbook.write(response.getOutputStream());
        } catch (Exception e) {
            log.error("导出excel错误");
            log.error(e.getMessage(), e);
        }
    }

    private static String adapterFileName(String fileName, ExportParams exportParams) {
        if(StringUtils.isEmpty(fileName)){
             throw  new RuntimeException("参数不能为空");
        }
        fileName = fileName.toLowerCase();
        if (fileName.lastIndexOf(BIG_FILE_EXT_FILE_NAME_XLSX) > 0) {
            exportParams.setType(ExcelType.XSSF);
            return fileName;
        } if (fileName.lastIndexOf(BIG_FILE_EXT_FILE_NAME_XLS) > 0) {
            exportParams.setType(ExcelType.HSSF);
            return fileName;
        }
        fileName = fileName + BIG_FILE_EXT_FILE_NAME_XLSX;
        exportParams.setType(ExcelType.XSSF);
        return fileName;
    }

    private static Workbook exportExcel(ExportParams exportParams, Class<?> pojoClass, Collection<?> dataSet) {
        if (CollectionUtils.isEmpty(dataSet)) {
            return ExcelExportUtil.exportBigExcel(exportParams, pojoClass, new ArrayList<>());
        }
        if (dataSet.size() > BIG_DATA_EXPORT_MAX) {
            throw  new RuntimeException("请求禁止");
        }
        Workbook workbook = null;
        if (dataSet.size() > BIG_DATA_EXPORT_MIN) {
            log.info("文件过大采用大文件导出：" + dataSet.size());
            for (int i = 0; i < (dataSet.size() / BIG_DATA_EXPORT_MIN + 1) && dataSet.size() > 0; i++) {
                log.info("当前切片：" + i * BIG_DATA_EXPORT_MIN + "-" + (i + 1) * BIG_DATA_EXPORT_MIN);
                List<?> update = dataSet.stream().skip(i * BIG_DATA_EXPORT_MIN).limit(BIG_DATA_EXPORT_MIN).collect(Collectors.toList());
                workbook = ExcelExportUtil.exportBigExcel(exportParams, pojoClass, update);
            }
            ExcelExportUtil.closeExportBigExcel();
        } else {
            workbook = ExcelExportUtil.exportExcel(exportParams, pojoClass, dataSet);
        }
        return workbook;
    }

}
