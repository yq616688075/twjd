package com.yliuliu.common.util;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by WANGBING15 on 2016/8/27.
 * <p>
 * UPDATE BY CHENHM8 ON 2018-10-31
 */
public class HttpUtils {
    private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    private static String[] IEBrowserSignals = {"MSIE", "Trident", "Edge"};

    public static boolean isMSBrowser(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        for (String signal : IEBrowserSignals) {
            if (userAgent.contains(signal))
                return true;
        }
        return false;
    }

    /**
     * @param apiUrl
     * @param json
     * @param proxyEnable
     * @param proxyHost
     * @param proxyPort
     * @return
     */
    public static String doPost(String apiUrl, Object json, String proxyEnable, String proxyHost, int proxyPort) {
        //接口地址
        PostMethod postMethod = new PostMethod(apiUrl);
        //设置头部
        postMethod.setRequestHeader("Header-Content-Type", "application/soap+xml;charset=UTF-8");
        postMethod.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
        postMethod.setRequestBody(json.toString());
        HttpClient httpClient = new HttpClient();
        StringBuilder xmlResult = new StringBuilder();
        try {
            //需要设置代理模式请求
            if (StringUtils.isNotEmpty(proxyEnable) && proxyEnable.equals("true")) {
                httpClient.getHostConfiguration().setProxy(proxyHost, proxyPort);
            }
            httpClient.executeMethod(postMethod);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    postMethod.getResponseBodyAsStream(), "UTF-8"));
            String tmp = null;
            while ((tmp = reader.readLine()) != null) {
                xmlResult.append(tmp);
            }
            String xmlStr = xmlResult.toString();
            logger.info("pay unified order result: " + xmlStr);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return xmlResult.toString();
    }

    /**
     * @param apiUrl
     * @param json
     * @return
     */
    public static String doPost(String apiUrl, Object json) {
        //接口地址
        PostMethod postMethod = new PostMethod(apiUrl);
        //设置头部
        postMethod.setRequestHeader("Header-Content-Type", "application/soap+xml;charset=UTF-8");
        postMethod.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
        postMethod.setRequestBody(json.toString());
        HttpClient httpClient = new HttpClient();
        StringBuilder result = new StringBuilder();
        try {
            httpClient.executeMethod(postMethod);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    postMethod.getResponseBodyAsStream(), "UTF-8"));
            String tmp = null;
            while ((tmp = reader.readLine()) != null) {
                result.append(tmp);
            }
            String xmlStr = result.toString();
            logger.info("pay unified order result: " + xmlStr);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return result.toString();
    }
}
