package com.yliuliu.common.util;

import com.overzealous.remark.Options;
import com.overzealous.remark.Remark;

/**
 * @description:
 * @author: yuanquan3
 * @version: 1.0
 * @date: 2020-4-20
 */
public class HtmlConevrtMarkDownUtils {
    private static final Remark remark = new Remark(Options.pegdownAllExtensions());

    public static Remark getRemark(){
        return remark;
    }

    private HtmlConevrtMarkDownUtils() {
    }

    public static String html2Markdown(String html){
        return remark.convertFragment(html);
    }
}
