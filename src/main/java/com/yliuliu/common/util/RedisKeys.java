package com.yliuliu.common.util;


import com.yliuliu.common.base.StringUtil;

/**
 * Created by CHENMK3 on 2018-7-9
 */
public class RedisKeys {
    private final static String KEY_PREFIX_COMMON_DATAATTRIBUTION_NO = "saas:common:dataAttribution:no:";

    private final static String KEY_PREFIX_COMMON_DATADICTIONARY_NO = "saas:common:dataDictionary:no:";

    private final static String KEY_PREFIX_COMMON_SAASROURCE_NO = "saas:common:saasResource:no:";

    /**
     * 获取数据字典归属key值
     * 创建时间 2018-7-9 9:04
     *
     * @param appKey
     * @param dataAttributionCode
     * @return
     * @author 陈满坤
     */
    public static String getDataAttributionNoKey(String appKey, String dataAttributionCode) {
        if (StringUtil.isPresent(appKey)) {
            return KEY_PREFIX_COMMON_DATAATTRIBUTION_NO + appKey + ":" + dataAttributionCode;
        }
        return KEY_PREFIX_COMMON_DATAATTRIBUTION_NO + dataAttributionCode;
    }

    /**
     * 获取数据字典key值
     * 创建时间 2018-7-9 9:04
     *
     * @param appKey
     * @param dataDictionaryCode
     * @return
     * @author 陈满坤
     */
    public static String getDataDictionaryNoKey(String appKey, String dataDictionaryCode) {
        if (StringUtil.isPresent(appKey)) {
            return KEY_PREFIX_COMMON_DATADICTIONARY_NO + appKey + ":" + dataDictionaryCode;
        }
        return KEY_PREFIX_COMMON_DATADICTIONARY_NO + dataDictionaryCode;
    }

    /**
     * @Description: 获取objId对应objIdType的所有资源
     * @Param: [appKey, dataResourceObjId, dataResourceObjIdType]
     * @return: java.lang.String
     * @Author: CHENMK3
     * @Date: 2018-11-27
     */
    public static String getSaasResourceNoKey(String appKey, String dataResourceObjId, Integer dataResourceObjIdType) {
        if (StringUtil.isPresent(appKey)) {
            return KEY_PREFIX_COMMON_SAASROURCE_NO + appKey + ":" + dataResourceObjIdType + ":" + dataResourceObjId;
        }
        return KEY_PREFIX_COMMON_SAASROURCE_NO + dataResourceObjIdType + ":" + dataResourceObjId;
    }
}
