package com.yliuliu.common.util.zkLock.annotation;

import java.lang.annotation.*;

/**
 * 创建时间 2018-05-02 16:39
 *
 * @author 陆国鸿
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DistributedLock {

    /**
     * 锁名称，为空使用方法全名称
     * @return
     */
    String lockName() default "";

    /**
     * 锁名字名称-通过spel实现，如：#demo.id，#demo.getId()
     * @return
     */
    String lockNameSubSpel() default "";

    /**
     * 取锁超时时间
     * @return
     */
    int timeoutMiliseconds() default 30000;

    /**
     * zk中锁的下级目录，首目录在DistributedLocker已经预先定义
     * @return
     */
    String appendRootLock() default "";

    /**
     * 是否尝试获取一次，不等待取锁，获取不到马上响应错误
     * @return
     */
    boolean getOnce() default false;

}
