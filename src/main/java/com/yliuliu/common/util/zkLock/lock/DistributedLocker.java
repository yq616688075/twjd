package com.yliuliu.common.util.zkLock.lock;

import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * 创建时间 2018-03-03 16:15
 *
 * @author 陆国鸿
 */
public class DistributedLocker implements Lock, Watcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(DistributedLocker.class);

    private ZooKeeper zk = null;
    // 根节点
    private String ROOT_LOCK = "/saas-spring-boot/locks";
    // 竞争的资源
    private String lockName;
    // 等待的前一个锁
    private String WAIT_LOCK;
    // 当前锁
    private String CURRENT_LOCK;
    // 计数器
    private CountDownLatch countDownLatch;

    private CountDownLatch connectedSemaphore;

    private Integer sessionTimeout = 30000;

    private List<Exception> exceptionList = new ArrayList<Exception>();

    /**
     * 配置分布式锁
     * @param config 连接的url
     * @param appendRootLock 追加根节点
     * @param lockName 竞争资源
     */
    public DistributedLocker(String config, String appendRootLock, String lockName, Integer timeoutMiliseconds) {
        if (!StringUtils.isBlank(appendRootLock)) {
            this.ROOT_LOCK = String.format("%s/%s", this.ROOT_LOCK, appendRootLock);
        }
        this.lockName = lockName;
        if (null != timeoutMiliseconds) {
            this.sessionTimeout = timeoutMiliseconds;
        }
        try {
            // 连接zookeeper
            this.connectedSemaphore = new CountDownLatch(1);
            zk = new ZooKeeper(config, sessionTimeout, this);
            this.connectedSemaphore.await();
            StringBuilder rootPath = new StringBuilder();
            for (String path : ROOT_LOCK.split("/")) {
                if (!StringUtils.isBlank(path)) {
                    rootPath.append("/").append(path);
                    String checkPath = rootPath.toString();
                    Stat stat = zk.exists(checkPath, false);
                    if (null == stat) {
                        // 如果根节点不存在，则创建根节点
                        zk.create(checkPath, new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("锁创建失败");
            LOGGER.error(e.getMessage(), e);
            if (null != this.zk) {
                try {
                    zk.close();
                } catch (InterruptedException e1) {
                    LOGGER.error("锁生成失败时，关闭zk出错");
                }
            }
            throw new LockException(e);
        }
    }

    // 节点监视器
    @Override
    public void process(WatchedEvent event) {
        if (Event.KeeperState.SyncConnected == event.getState()) {
            // 在最开始使用，只是用一次
            if (null != this.connectedSemaphore) {
                this.connectedSemaphore.countDown();
                this.connectedSemaphore = null;
            }
        }
        if (this.countDownLatch != null) {
            this.countDownLatch.countDown();
        }
    }

    @Override
    public void lock() {
        if (exceptionList.size() > 0) {
            throw new LockException(exceptionList.get(0));
        }
        try {
            if (this.tryLock()) {
                LOGGER.debug("thread[{}] get locker -> [{}]", Thread.currentThread().getName(), lockName);
                return;
            } else {
                // 等待锁
                waitForLock(WAIT_LOCK, sessionTimeout);
            }
        } catch (Exception e) {
            throw new LockException(e);
        }
    }

    @Override
    public boolean tryLock() {
        try {
            String splitStr = "_lock_";
            if (lockName.contains(splitStr)) {
                throw new LockException("锁名有误");
            }
            // 创建临时有序节点
            CURRENT_LOCK = zk.create(ROOT_LOCK + "/" + lockName + splitStr, new byte[0],
                    ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
            LOGGER.debug("{} already exists", CURRENT_LOCK);
            // 取所有子节点
            List<String> subNodes = zk.getChildren(ROOT_LOCK, false);
            // 取出所有lockName的锁
            List<String> lockObjects = new ArrayList<String>();
            for (String node : subNodes) {
                String _node = node.split(splitStr)[0];
                if (_node.equals(lockName)) {
                    lockObjects.add(node);
                }
            }
            Collections.sort(lockObjects);
            LOGGER.debug("thread [{}]'s lock is {}", CURRENT_LOCK);
            // 若当前节点为最小节点，则获取锁成功
            if (CURRENT_LOCK.equals(ROOT_LOCK + "/" + lockObjects.get(0))) {
                return true;
            }

            // 若不是最小节点，则找到自己的前一个节点
            String prevNode = CURRENT_LOCK.substring(CURRENT_LOCK.lastIndexOf("/") + 1);
            WAIT_LOCK = lockObjects.get(Collections.binarySearch(lockObjects, prevNode) - 1);
        } catch (Exception e) {
            throw new LockException(e);
        }
        return false;
    }

    @Override
    public boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
        try {
            if (this.tryLock()) {
                return true;
            }
            return waitForLock(WAIT_LOCK, timeout);
        } catch (Exception e) {
            throw new LockException(e);
        }
    }

    // 等待锁
    private boolean waitForLock(String prev, long waitTime) throws KeeperException, InterruptedException {
        Stat stat = zk.exists(ROOT_LOCK + "/" + prev, true);

        if (stat != null) {
            LOGGER.debug("thread [{}] waiting for lock [{}/{}]", Thread.currentThread().getName(), ROOT_LOCK, prev);
            this.countDownLatch = new CountDownLatch(1);
            // 计数等待，若等到前一个节点消失，则precess中进行countDown，停止等待，获取锁
            this.countDownLatch.await(waitTime, TimeUnit.MILLISECONDS);
            this.countDownLatch = null;
            LOGGER.debug("thread [{}] get locker", Thread.currentThread().getName());
        }
        return true;
    }

    @Override
    public void unlock() {
        try {
            if (!StringUtils.isBlank(CURRENT_LOCK)) {
                LOGGER.debug("unlock [{}]", CURRENT_LOCK);
                zk.delete(CURRENT_LOCK, -1);
                CURRENT_LOCK = null;
                zk.close();
            }
        } catch (Exception e) {
            throw new LockException(e);
        }
    }

    public void releaseLock() {
        try {
            LOGGER.debug("releaseLock close zk [{}]", CURRENT_LOCK);
            if (this.zk != null && zk.getState() != ZooKeeper.States.CLOSED) {
                zk.close();
            }
        } catch (Exception e) {
            throw new LockException(e);
        }
    }

    @Override
    public Condition newCondition() {
        return null;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        this.lock();
    }


    public class LockException extends RuntimeException {
        private static final long serialVersionUID = 1L;
        public LockException(String e){
            super(e);
        }
        public LockException(Exception e){
            super(e);
        }
    }
}
