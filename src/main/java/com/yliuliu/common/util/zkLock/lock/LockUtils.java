package com.yliuliu.common.util.zkLock.lock;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 创建时间 2018-03-03 16:20
 *
 * @author 陆国鸿
 */
@Component
public class LockUtils {
    @Value("${common.zookeeper.hosts:}")
    private String zookeeperConnectString;

    public DistributedLocker getDisTributedLock(String key, Integer timeout) {
        return new DistributedLocker(zookeeperConnectString, null, key, timeout);
    }

    public DistributedLocker getDisTributedLock(String appendRootLock, String key, Integer timeout) {
        return new DistributedLocker(zookeeperConnectString, appendRootLock, key, timeout);
    }

    public DistributedLocker getDisTributedLock(String key) {
        return this.getDisTributedLock(key, null);
    }
}
