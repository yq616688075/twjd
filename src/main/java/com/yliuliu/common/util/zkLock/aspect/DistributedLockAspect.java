package com.yliuliu.common.util.zkLock.aspect;

import com.yliuliu.common.util.zkLock.annotation.DistributedLock;
import com.yliuliu.common.util.zkLock.lock.DistributedLocker;
import com.yliuliu.common.util.zkLock.lock.LockUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.spi.ErrorCode;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 创建时间 2018-05-02 16:53
 *
 * @author 陆国鸿
 */
@Aspect
@Component
public class DistributedLockAspect {

    @Autowired
    private LockUtils lockUtils;

    @Pointcut("@annotation(com.yliuliu.common.util.zkLock.annotation.DistributedLock)")
    public void executePointcut(){

    }

    @Around("executePointcut()")
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        if (!(signature instanceof MethodSignature)) {
            throw new RuntimeException("DistributedLockAnnotation's target must be method");
        }
        MethodSignature methodSignature = (MethodSignature)signature;
        Method method = methodSignature.getMethod();
        DistributedLock dla = method.getAnnotation(DistributedLock.class);
        String lockName = dla.lockName();
        String appendRootLock = dla.appendRootLock();
        int timeout = dla.timeoutMiliseconds();
        boolean getOnce = dla.getOnce();
        String lockNameSubSpel = dla.lockNameSubSpel();
        DistributedLocker locker = null;
        boolean isLock = false;
        try {
            StringBuilder finalLockName = new StringBuilder();
            if (StringUtils.isBlank(lockName)) {
                finalLockName.append(method.getDeclaringClass().getName())
                        .append(".")
                        .append(method.getName());
            } else {
                finalLockName.append(lockName);
            }
            if (StringUtils.isNotBlank(lockNameSubSpel)) {
                Object[] args = joinPoint.getArgs();
                if (null != args && args.length > 0) {
                    String[] paramNames = ((CodeSignature) joinPoint.getSignature()).getParameterNames();
                    StandardEvaluationContext context = new StandardEvaluationContext();
                    for (int i = 0; i < args.length; i++) {
                        context.setVariable(paramNames[i], args[i]);
                    }
                    SpelExpressionParser parser = new SpelExpressionParser();
                    Object lockNameSub = parser.parseExpression(lockNameSubSpel).getValue(context);
                    if (null != lockNameSub && lockNameSub.toString().length() > 0) {
                        finalLockName.append("-").append(lockNameSub.toString());
                    }
                }
            }
            locker = this.lockUtils.getDisTributedLock(appendRootLock, finalLockName.toString(), timeout);
            if (getOnce) {
                isLock = locker.tryLock();
                if (!isLock) {
                    throw  new RuntimeException("用户操作中，请勿重复操作");
                }
            } else {
                locker.lock();
                isLock = true;
            }
            return joinPoint.proceed(joinPoint.getArgs());
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (null != locker) {
                if (isLock) {
                    locker.unlock();
                }
                locker.releaseLock();
            }
        }
    }

}
