package com.yliuliu.common.util;

//import com.meicloud.paas.core.util.sec.Base64Utils;
import lombok.Data;
//import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

/**
 * 图片处于工具类
 * Created by LIANGJJ15 on 2019/1/18.
 */
@Data
public class ImageUtil {

//    private static Logger logger = LoggerFactory.getLogger(ImageUtil.class);
//
//    /**
//     * 默认图片压缩比例
//     */
//    public final static double DEFAULT_SCALE = 1f;
//
//    /**
//     * 默认图片压缩质量
//     */
//    public final static float DEFAULT_OUT_PUT_QUALITY = 0.5f;
//
//
//    @Data
//    public static class ImageSize {
//        private Integer width;
//        private Integer height;
//    }
//
//    @Data
//    public static class CompressImageParam {
//        private double scale;
//        private float outputQuality;
//    }
//
//    /**
//     * 获取图片尺寸大小
//     *
//     * @param bytes 字节数组
//     * @return
//     */
//    public static ImageSize getImageSize(byte[] bytes) {
//        if (bytes != null) {
//            ImageSize imageSize = new ImageSize();
//            InputStream inputStream = new ByteArrayInputStream(bytes);
//            try {
//                BufferedImage bufferedImage = ImageIO.read(inputStream);
//                imageSize.setWidth(bufferedImage.getWidth());
//                imageSize.setHeight(bufferedImage.getHeight());
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                CloseUtil.closeSilently(inputStream);
//            }
//            return imageSize;
//        }
//        return null;
//    }
//
//    private static CompressImageParam buildDefault(double scale, float outputQuality) {
//        CompressImageParam compressImageParam = new CompressImageParam();
//        if (scale == 0f) {
//            scale = ImageUtil.DEFAULT_SCALE;
//        }
//
//        if (outputQuality == 0f) {
//            outputQuality = ImageUtil.DEFAULT_OUT_PUT_QUALITY;
//        }
//
//        compressImageParam.setOutputQuality(outputQuality);
//        compressImageParam.setScale(scale);
//
//        return compressImageParam;
//    }
//
//    /**
//     * 压缩图片（File）
//     *
//     * @param file          需要压缩的图片文件
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return 字节数组
//     */
//    public static byte[] compressImage(File file, double scale, float outputQuality) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        CompressImageParam compressImageParam = ImageUtil.buildDefault(scale, outputQuality);
//
//        try {
//            Thumbnails.of(file)
//                    .scale(compressImageParam.getScale())
//                    .outputQuality(compressImageParam.getOutputQuality())
//                    .toOutputStream(byteArrayOutputStream);
//        } catch (Exception e) {
//            logger.debug("[File]compress image fail message : {}", e.getMessage());
//            return null;
//        }
//
//        return byteArrayOutputStream.toByteArray();
//    }
//
//    /**
//     * 压缩图片
//     *
//     * @param imageUrl      需要压缩的网络文件地址
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return 字节数组
//     */
//    public static byte[] compressImage(String imageUrl, double scale, float outputQuality) {
//        URL url = null;
//        try {
//            url = new URL(imageUrl);
//        } catch (Exception e) {
//            logger.debug("build Url fail message : {}", imageUrl);
//        }
//        return ImageUtil.compressImage(url, scale, outputQuality);
//    }
//
//    /**
//     * 压缩图片（URL）
//     *
//     * @param url           网络URL对象
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return 字节数组
//     */
//    public static byte[] compressImage(URL url, double scale, float outputQuality) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        CompressImageParam compressImageParam = ImageUtil.buildDefault(scale, outputQuality);
//
//        try {
//            Thumbnails.of(url)
//                    .scale(compressImageParam.getScale())
//                    .outputQuality(compressImageParam.getOutputQuality())
//                    .toOutputStream(byteArrayOutputStream);
//        } catch (Exception e) {
//            logger.debug("[URL]compress image fail message : {}", e.getMessage());
//            return null;
//        }
//
//        return byteArrayOutputStream.toByteArray();
//    }
//
//    /**
//     * 压缩图片（InputStream）
//     *
//     * @param inputStream   文件输入流
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return 字节数组
//     */
//    public static byte[] compressImage(InputStream inputStream, double scale, float outputQuality) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        CompressImageParam compressImageParam = ImageUtil.buildDefault(scale, outputQuality);
//
//        try {
//            Thumbnails.of(inputStream)
//                    .scale(compressImageParam.getScale())
//                    .outputQuality(compressImageParam.getOutputQuality())
//                    .toOutputStream(byteArrayOutputStream);
//        } catch (Exception e) {
//            logger.debug("[InputStream]compress image fail message : {}", e.getMessage());
//            return null;
//        }
//
//        return byteArrayOutputStream.toByteArray();
//    }
//
//    /**
//     * 压缩图片（BufferedImage）
//     *
//     * @param bufferedImage 图片缓冲流
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return 字节数组
//     */
//    public static byte[] compressImage(BufferedImage bufferedImage, double scale, float outputQuality) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        CompressImageParam compressImageParam = ImageUtil.buildDefault(scale, outputQuality);
//
//        try {
//            Thumbnails.of(bufferedImage)
//                    .scale(compressImageParam.getScale())
//                    .outputQuality(compressImageParam.getOutputQuality())
//                    .toOutputStream(byteArrayOutputStream);
//        } catch (Exception e) {
//            logger.debug("[BufferedImage]compress image fail message : {}", e.getMessage());
//            return null;
//        }
//
//        return byteArrayOutputStream.toByteArray();
//    }
//
//
//    /**
//     * 压缩图片（File）
//     *
//     * @param file          图片缓冲流
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return base64字符串
//     */
//    public static String compressImageToBase64(File file, double scale, float outputQuality) {
//        String base64 = "";
//        try {
//            base64 = Base64Utils.encode(ImageUtil.compressImage(file, scale, outputQuality));
//        } catch (Exception e) {
//            logger.debug("[File]compress image to base64 fail message : {}", e.getMessage());
//        }
//
//        return base64;
//    }
//
//    /**
//     * 压缩图片（imageUrl）
//     *
//     * @param imageUrl      网络图片地址
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return base64字符串
//     */
//    public static String compressImageToBase64(String imageUrl, double scale, float outputQuality) {
//        String base64 = "";
//        try {
//            base64 = Base64Utils.encode(ImageUtil.compressImage(imageUrl, scale, outputQuality));
//        } catch (Exception e) {
//            logger.debug("[imageUrl]compress image to base64 fail message : {}", e.getMessage());
//        }
//
//        return base64;
//    }
//
//    /**
//     * 压缩图片（URL）
//     *
//     * @param url           URL对象
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return base64字符串
//     */
//    public static String compressImageToBase64(URL url, double scale, float outputQuality) {
//        String base64 = "";
//        try {
//            base64 = Base64Utils.encode(ImageUtil.compressImage(url, scale, outputQuality));
//        } catch (Exception e) {
//            logger.debug("[URL]compress image to base64 fail message : {}", e.getMessage());
//        }
//
//        return base64;
//    }
//
//    /**
//     * 压缩图片（InputStream）
//     *
//     * @param inputStream   输入流
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return base64字符串
//     */
//    public static String compressImageToBase64(InputStream inputStream, double scale, float outputQuality) {
//        String base64 = "";
//        try {
//            base64 = Base64Utils.encode(ImageUtil.compressImage(inputStream, scale, outputQuality));
//        } catch (Exception e) {
//            logger.debug("[URL]compress image to base64 fail message : {}", e.getMessage());
//        }
//
//        return base64;
//    }
//
//    /**
//     * 压缩图片（BufferedImage）
//     *
//     * @param bufferedImage 图片BufferedImage
//     * @param scale         压缩比例（0-1）
//     * @param outputQuality 压缩比例
//     * @return base64字符串
//     */
//    public static String compressImageToBase64(BufferedImage bufferedImage, double scale, float outputQuality) {
//        String base64 = "";
//        try {
//            base64 = Base64Utils.encode(ImageUtil.compressImage(bufferedImage, scale, outputQuality));
//        } catch (Exception e) {
//            logger.debug("[BufferedImage]compress image to base64 fail message : {}", e.getMessage());
//        }
//
//        return base64;
//    }
}