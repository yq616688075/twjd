package com.yliuliu.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 容器工具类
 */
public final class CollectionsUtil {

    private static Logger logger = LoggerFactory.getLogger(CollectionsUtil.class);

    /**
     * 读取map中的值，防止空指针异常
     * @param map
     * @param key
     * @return 返回获取的值（String 类型）
     */
    public static String getMapValueString(Map map, String key) {
        return map.get(key) == null ? null : map.get(key).toString();
    }

    /**
     * 读取map中的值，防止空指针异常(获取数组中的第一个值)
     * @param map
     * @param key
     * @return 返回获取的值（String 类型）
     */
    public static String getMapFristValueString(Map map, String key) {
        String ret = null;
        if (map.get(key) != null)  {
            Object obj = map.get(key);
            if (obj.getClass().isArray()) {
                Object[] objs = (Object[])obj;
                ret = objs[0].toString();
            }
        }
        return ret;
    }

    /**
     * 读取List对象中某一个属性的值
     * @param list
     * @param c
     * @param field
     * @return 返回属性值List
     */
    public static List<String> getFieldValueList(List<?> list, Class<?> c, String field) {
        List<String> fieldList = new ArrayList<>();
        if (!list.isEmpty()) {
            Field[] fields = c.getDeclaredFields();
            int pos;
            for (pos = 0; pos < fields.length; pos++) {
                if (field.equals(fields[pos].getName())) {
                    break;
                }
            }

            for (Object o : list) {
                try {
                    fields[pos].setAccessible(true);
                    fieldList.add(fields[pos].get(o).toString());
                } catch (Exception e) {
                    logger.error("error--------" + "Reason is:" + e.getMessage());
                }
            }
        }
        return fieldList;
    }
}
