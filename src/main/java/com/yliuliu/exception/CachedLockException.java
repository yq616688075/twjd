package com.yliuliu.exception;

/**
 * Created by admin on 2019/7/26.
 */
public class CachedLockException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public CachedLockException() {
        super();
    }

    public CachedLockException(String message) {
        super(message);
    }

    public CachedLockException(String message, Throwable cause) {
        super(message, cause);
    }
}
